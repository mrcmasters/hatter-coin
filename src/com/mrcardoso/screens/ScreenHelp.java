package com.mrcardoso.screens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;

public class ScreenHelp {
	public static void render(Graphics g) {
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		int xAxios = Game.SCALE_6;
		int sizeFont = 20;
		int initialY = Game.SCALE_12;
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(new Color(0,0,0, 255));
		g2.fillRect(0, 0, xxx, yyy);
		
		g2.setFont(new Font("arial", Font.BOLD, sizeFont));
		g2.setColor(Color.ORANGE);
		g2.drawString("In Gaming:", xAxios, initialY);
		g2.setColor(new Color(255,255,255));
		g2.drawString("- The arrow keys your player walk", xAxios, initialY*2);
		g2.drawString("- Space key your player jump", xAxios, initialY*3);
		g2.drawString("- S key your player shoot", xAxios, initialY*4);
		g2.drawString("- ESC go to the menu", xAxios, initialY*5);
		g2.drawString("- To shoot in vertical you need press Enter then S key", xAxios, initialY*6);
		g2.setColor(Color.ORANGE);
		g2.drawString("In Menu", xAxios, initialY*7);
		g2.setColor(new Color(255,255,255));
		g2.drawString("- Arrow keys you move between the options", xAxios, initialY*8);
		g2.drawString("- Enter key you select the menu/option", xAxios, initialY*9);
		g2.drawString("- Esc key you go back to main menu", xAxios, initialY*10);
	}
}

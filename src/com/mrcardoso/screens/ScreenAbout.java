package com.mrcardoso.screens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;

public class ScreenAbout {
	public static void render(Graphics g) {
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(new Color(0,0,0, 255));
		g2.fillRect(0, 0, xxx, yyy);
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_16));
		g2.setColor(Color.ORANGE);
		g2.drawString("About", (int) (xxx/2.5), Game.SCALE_22);
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_8));
		g2.setColor(new Color(255,255,255));
		g2.drawString("Your mission, if accept.", (int) (xxx/3), Game.SCALE_40);
		g2.drawString("It's run the levels, collect coins, kill enemies", (int) (xxx/6), Game.SCALE_40+Game.SCALE_10);
		g2.drawString("and fun ;)", (int) (xxx/2.5), Game.SCALE_55+Game.SCALE_6);
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_4));
		g2.setColor(Color.ORANGE);
		g2.drawString("Sound effect and some sprites created by:", (int) Game.SCALE_10, (int)(yyy/2.1));
		g2.drawString("- www.kenney.nl", (int) Game.SCALE_10, (int) (yyy/1.9));
		g2.drawString("- SwissArcadeGameEntertainment", (int) Game.SCALE_10, (int) (yyy/1.8));
		g2.drawString("- opengameart@ignasd", (int) Game.SCALE_10, (int) (yyy/1.7));
		g2.drawString("- opengameart@ansimuz", (int) Game.SCALE_10, (int) (yyy/1.6));
		g2.drawString("- opengameart@PWL", (int) Game.SCALE_10, (int) (yyy/1.5));
		g2.drawString("- opengameart@CJHD2D", (int) Game.SCALE_10, (int) (yyy/1.43));
		g2.drawString("- opengameart@nene", (int) Game.SCALE_10, (int) (yyy/1.36));
		g2.drawString("- cynicmusic.com", (int) Game.SCALE_10, (int) (yyy/1.30));
	}
}

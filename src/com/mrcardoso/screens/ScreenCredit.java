package com.mrcardoso.screens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;
import com.mrcardoso.graphcs.Animation;

public class ScreenCredit {
	private static String[] typingTexts = {
		"Este Jogo chegou ao fim!",
		"Espero que tenha se divertido por aqui.",
		"Este jogo e uma prova de determinacao.",
		"Quando esta decidido a concluir algo...",
		"va em frente.",
		"e nao pare por nada, ate chegar la",
		"Obrigado por jogar <#",
		"Ate a próxima!"
		
	};
	public static void render(Graphics g) {
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, xxx, yyy);
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_16));
		g2.setColor(Color.ORANGE);
		g2.drawString("Creditos", (int) (xxx/3), Game.SCALE_22);
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_8));
		g2.setColor(new Color(255,255,255));
		
		Animation.typing(g2, typingTexts, Game.SCALE_10, (int) (yyy/3), Color.WHITE);
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_6));
		
		g2.setColor(Color.ORANGE);
		g2.drawString(Game.getScore()+" moeda(s) coletada(s).", Game.SCALE_30, (int)(yyy/2.4));
		g2.setColor(Color.RED );
		g2.drawString(Game.getEnemyKill()+" inimigo(s) derrotado(s).", (int) (xxx/2), (int)(yyy/2.4));
		
		g2.setColor(Color.WHITE);
		g2.drawString("Meus sinceros agradecimentos a:", (int) Game.SCALE_10, (int)(yyy/1.8));
		g2.setColor(new Color(90, 60, 160));
		g2.drawString("DankiCode: Que me forneceu a base de skills para criar este jogo.", (int) Game.SCALE_10, (int) (yyy/1.6));
		g2.setColor(Color.GRAY);
		g2.drawString("UnidayStudio: que me motivou a publicar e concluir este jogo.", (int) Game.SCALE_10, (int) (yyy/1.44));
		g2.setColor(Color.GREEN);
		g2.drawString("E Voce player que chegou ate aqui!!!", (int) Game.SCALE_10, (int) (yyy/1.32));
	}
}

package com.mrcardoso.screens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;
import com.mrcardoso.entities.Player;

public class ScreenLevel {
	private static int frames =0, index = 0, maxFrames = 30, maxIndex = Player.jumpSpriteSize();
	private static int transition =0, transitionMax = 60*4;
	
	public static void render(Graphics g) {
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		String label = Game.isLastLevel() ? "Final Level" : ("Level "+Game.getLevel());
		
		if(transition>transitionMax) {
			transition=0;
			frames=0;
			Game.reset(false);
			Game.getState().setStateRunning();
			return;
		} else {
			transition++;
		}
		
		frames++;
		if(frames==maxFrames) {
			frames=0;
			index++;
			if(index==maxIndex) {
				index=0;
			}
		}
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(new Color(90,45,180, 255));
		g2.fillRect(0, 0, xxx, yyy);
		
		g2.drawImage(Player.getJumpSprite(index), (int) (xxx/2.4), (int) (yyy/4), Game.OZ*4, Game.OZ*4, null);
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_16));
		g2.setColor(new Color(224, 205,137));
		g2.drawString(label, (int) (xxx/3) - label.length(), (int) (yyy/2));
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_8));
		g2.setColor(new Color(255,255,255));
		g2.drawString("Press ENTER or WAIT...", xxx/3, (int)(yyy/1.6));
	}
}

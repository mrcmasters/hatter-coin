package com.mrcardoso.screens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;
import com.mrcardoso.entities.Player;
import com.mrcardoso.graphcs.Animation;

public class ScreenLose {
	
	private static int frames =0, index = 0, maxFrames = 30, maxIndex = Player.spriteLoseSize();
	public static void render(Graphics g) {
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		
		frames++;
		if(frames==maxFrames) {
			frames=0;
			index++;
			if(index==maxIndex) {
				index=0;
			}
		}
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(new Color(26,26,26));
		g2.fillRect(0, 0, xxx, yyy);
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_20));
		g2.setColor(new Color(255,0,0));
		g2.drawString("Game Over", (int)(xxx/3.5), (yyy/2));
		
		g2.drawImage(Player.getSpriteLose(index), (int) (xxx/2.4), (int) (yyy/4), Game.OZ*5, Game.OZ*5, null);
		
		Animation.blink(g, ">>Press enter to restart<<", (int)(xxx/4), (int)(yyy/1.7), new Color(255,255,255));
	}
}

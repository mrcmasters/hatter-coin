package com.mrcardoso.screens;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.mrcardoso.core.Game;
import com.mrcardoso.entities.Entity;
import com.mrcardoso.graphcs.Menu;

public class Screen {
	private String screen;
	
	public void tick() {}
	
	public void render(Graphics g) {
		switch(this.screen) {
			case Menu.CODE_MORE_HOW_PLAY: ScreenHelp.render(g); break;
			case Menu.CODE_MORE_ABOUT: ScreenAbout.render(g); break;
			case Menu.CODE_MORE_CREDITS: ScreenCredit.render(g); break;
			case Menu.CODE_MORE_LOSE: ScreenLose.render(g); break;
			case Menu.CODE_MORE_LEVEL: ScreenLevel.render(g); break;
		}
		
		int yAxios = Game.getYcale();
		g.setFont(new Font("arial", Font.BOLD, Game.SCALE_8));
		g.setColor(Color.GRAY);
		g.drawString(Game.AUTHOR, (int) Game.SCALE_10, (int)(yAxios/1.1));
		g.drawImage(Entity.LOGO, (int) (Game.getXcale()/1.2), (int) (yAxios/1.3), Game.OZ*4, Game.OZ*4, null);
	}
	
	public void setScreen(String code) {
		this.screen = code;
	}
	
	public boolean isLevelScreen() {
		return this.screen == Menu.CODE_MORE_LEVEL;
	}
	public boolean isCreditScreen() {
		return this.screen == Menu.CODE_MORE_CREDITS;
	}
}

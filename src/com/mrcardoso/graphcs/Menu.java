package com.mrcardoso.graphcs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mrcardoso.core.Game;
//import com.mrcardoso.core.MiniMap;
import com.mrcardoso.core.Storage;

public class Menu
{
	/**
	 * [New Game]
	 * code for start a new game 
	 */
	private static final String CODE_NEW = "game_new";
	/**
	 * [Resume Game]
	 * code for return the game in the last pause state
	 */
	private static final String CODE_RESUME = "game_resume";
	/**
	 * [Restart Game]
	 * code for reset the game again
	 */
	private static final String CODE_RESTART = "game_restart";
	
	/**
	 * [More]
	 * code for reset the game again
	 */
	private static final String CODE_MORE = "game_more";
	/**
	 * [More->How to play]
	 * code for reset the game again
	 */
	public static final String CODE_MORE_HOW_PLAY = "game_how_play";
	/**
	 * [More->About]
	 * code for reset the game again
	 */
	public static final String CODE_MORE_ABOUT = "game_about";
	public static final String CODE_MORE_CREDITS = "game_screen_credits";
	public static final String CODE_MORE_LOSE = "game_screen_lose";
	public static final String CODE_MORE_LEVEL = "game_level_transition";
	
	/**
	 * [Configurations]
	 * code with item of menu with options
	 */
	private static final String CODE_CONFIG = "game_options";
	/**
	 * [Configurations->Song Enabled]
	 * Sub-menu of options to enable or disable the song of the game
	 */
	private static final String CODE_CONFIG_SG_ENABLED = "game_options_song";
	/**
	 * [Configurations->Song Enabled->Enabled]
	 * code for enabled the song in the game
	 */
	private static final String CODE_CONFIG_SG_ENABLED_1 = "game_options_song_yes";
	/**
	 * [Configurations->Song Enabled->Disabled]
	 * code for disabled the song in the game
	 */
	private static final String CODE_CONFIG_SG_ENABLED_0 = "game_options_song_no";
	/**
	 * [Configurations->Song Enabled->Only Soundtrack]
	 * code for disabled the song in the game
	 */
	private static final String CODE_CONFIG_SG_ENABLED_2 = "game_options_song_only_soundtrack";
	/**
	 * [Configurations->Song Enabled->Only SoundEffect]
	 * code for disabled the song in the game
	 */
	private static final String CODE_CONFIG_SG_ENABLED_3 = "game_options_song_only_effect";
	
	/**
	 * [Configurations->Has Time]
	 * Sub-menu of options to enable or disable the song of the game
	 */
	private static final String CODE_CONFIG_TIME_ENABLED = "game_options_time";
	/**
	 * [Configurations->Has Time->Yes]
	 * code for enabled the song in the game
	 */
	private static final String CODE_CONFIG_TIME_ENABLED_1 = "game_options_time_yes";
	/**
	 * [Configurations->Has Time->No]
	 * code for disabled the song in the game
	 */
	private static final String CODE_CONFIG_TIME_ENABLED_0 = "game_options_time_no";
	/**
	 * [Configurations->Difficulty]
	 * Sub-menu of options to change game difficulty
	 */
	private static final String CODE_CONFIG_DIFFICULTY = "game_options_difficulty";
	/**
	 * [Configurations->Difficulty->EASY]
	 * code for set difficulty as easy
	 */
	private static final String CODE_CONFIG_DIFFICULTY_1 = "game_options_difficulty_1";
	/**
	 * [Configurations->Difficulty->Normal]
	 * code for set difficulty as normal
	 */
	private static final String CODE_CONFIG_DIFFICULTY_2 = "game_options_difficulty_2";
	/**
	 * [Configurations->Difficulty->Hard]
	 * code for set difficulty as hard
	 */
	private static final String CODE_CONFIG_DIFFICULTY_3 = "game_options_difficulty_3";
	
	
	/**
	 * [Save Game]
	 * code with item of menu with save game
	 */
	private static final String CODE_SAVE = "game_save";
	/**
	 * [Save Game->storage1]
	 * code with storage option one
	 */
	private static final String CODE_SAVE_OPTION_1 = "save_one";
	/**
	 * [Save Game->storage 3]
	 * code with storage option two
	 */
	private static final String CODE_SAVE_OPTION_2 = "save_two";
	/**
	 * [Save Game->storage 3]
	 * code with storage option three
	 */
	private static final String CODE_SAVE_OPTION_3 = "save_three";
	/**
	 * [Load Game]
	 * code with item of menu with load the game
	 */
	private static final String CODE_LOAD = "game_load";
	/**
	 * [Load Game->storage 1]
	 * code with storage load option one
	 */
	private static final String CODE_LOAD_OPTION_1 = "load_one";
	/**
	 * [Load Game->storage 2]
	 * code with storage load option two
	 */
	private static final String CODE_LOAD_OPTION_2 = "load_two";
	/**
	 * [Load Game->storage 3]
	 * code with storage load option three
	 */
	private static final String CODE_LOAD_OPTION_3 = "load_three";
	/**
	 * [Choose Phase]
	 * code for choose phase of the game
	 */
	private static final String CODE_CHOOSE = "game_choose";
	private static final String CODE_CHOOSE_PHASE_1 = "1";
	private static final String CODE_CHOOSE_PHASE_2 = "2";
	private static final String CODE_CHOOSE_PHASE_3 = "3";
	private static final String CODE_CHOOSE_PHASE_4 = "4";
	private static final String CODE_CHOOSE_PHASE_5 = "5";
	private static final String CODE_CHOOSE_PHASE_6 = "6";
	private static final String CODE_CHOOSE_PHASE_7 = "7";
	private static final String CODE_CHOOSE_PHASE_8 = "8";
	private static final String CODE_CHOOSE_PHASE_9 = "9";
	private static final String CODE_CHOOSE_PHASE_10 = "10";
	
	private boolean up, down,right,left, choosed, deleting, paused, allowSubItems;
	private boolean[] saveOptions = {false,false,false}; 
	private String chooseCode;
	private int navigateItem = 0;
	private int maxItems = 0;
	private int timeropacity = 255;
	private ArrayList<String> itemCodes;
	
	private ArrayList<String> generageItems()
	{
		ArrayList<String> items = new ArrayList<String>();
		this.itemCodes =  new ArrayList<String>();
		
		if(!this.allowSubItems)
		{
			if(this.paused) {
				items.addAll(Arrays.asList("Resume Game"));
				this.itemCodes.addAll(Arrays.asList(Menu.CODE_RESUME));
			}
			items.addAll(Arrays.asList("New Game", "Restart Game", "Options", "How To Play", "More"));
			this.itemCodes.addAll(Arrays.asList(Menu.CODE_NEW, Menu.CODE_RESTART, Menu.CODE_CONFIG, Menu.CODE_MORE_HOW_PLAY, Menu.CODE_MORE_ABOUT));			
		}
		else
		{
			switch(this.chooseCode) {
				case Menu.CODE_SAVE: 
					items.addAll(this.proccessItems("Save Game"));
					this.itemCodes.addAll(Arrays.asList(CODE_SAVE_OPTION_1, CODE_SAVE_OPTION_2,CODE_SAVE_OPTION_3));
					break; 
				case Menu.CODE_LOAD:
					items.addAll(this.proccessItems("Load Game"));
					this.itemCodes.addAll(Arrays.asList(CODE_LOAD_OPTION_1, CODE_LOAD_OPTION_2,CODE_LOAD_OPTION_3));
					break;
				case Menu.CODE_CHOOSE:
					for(int l = 0; l < Game.MAX_LEVEL; l++) {
						String level = Integer.toString(l+1);
						items.add("Level "+level);
						this.itemCodes.add(level);
					}
					break;
				case Menu.CODE_CONFIG_DIFFICULTY:
					items.addAll(Arrays.asList("Easy", "Normal", "Hard"));
					this.itemCodes.addAll(Arrays.asList(CODE_CONFIG_DIFFICULTY_1,CODE_CONFIG_DIFFICULTY_2, CODE_CONFIG_DIFFICULTY_3));
					break;
				case Menu.CODE_CONFIG: 
					items.addAll(Arrays.asList("Sound and Effect", "Has Time", "Difficulty"));
					this.itemCodes.addAll(Arrays.asList(CODE_CONFIG_SG_ENABLED, CODE_CONFIG_TIME_ENABLED,  CODE_CONFIG_DIFFICULTY));
					break;
				case Menu.CODE_CONFIG_SG_ENABLED: 
					items.addAll(Arrays.asList("Enabled", "Disabled", "Only SoundTrack", "Only EffectSound"));
					this.itemCodes.addAll(Arrays.asList(CODE_CONFIG_SG_ENABLED_1, CODE_CONFIG_SG_ENABLED_0, CODE_CONFIG_SG_ENABLED_2, CODE_CONFIG_SG_ENABLED_3));
					break;
					
				case Menu.CODE_CONFIG_TIME_ENABLED: 
					items.addAll(Arrays.asList("Yes", "No"));
					this.itemCodes.addAll(Arrays.asList(CODE_CONFIG_TIME_ENABLED_1, CODE_CONFIG_TIME_ENABLED_0));
					break;
				/**
				 * Disabled this resource
				case Menu.CODE_CONFIG_MM_OPACITY:
					MiniMap map = Game.world.getMiniMap();
					if(this.right) {
						this.right = false;
						map.incrementTransparency();
					} else if(this.left) {
						this.left = false;
						map.decrementTransparency();
					}
					String text = "___________";
					float trans = map.getTransparency();
					float trans2 = (float) 0.0;
					
					for(int i = 0; i < text.length(); i++) {
						if(trans == trans2) {
							text = text.substring(0,i)+i+text.substring(i+1);
						}
						trans2 = Game.formatValue(trans2 + 0.1, "#.#");
					}
					items.add(text);
					this.itemCodes.add(CODE_CONFIG_MM_OPACITY_0);
					break;
					*/
			}
		}
		return items;
	}
	
	private void chooseItem()
	{
		if(this.navigateItem < 0 ||  this.navigateItem > (this.itemCodes.size()-1)){
			return;
		}
		String code = this.itemCodes.get(this.navigateItem);
		
		switch(code){
			// Codes with sub-items in the menu of game
			case Menu.CODE_SAVE:
			case Menu.CODE_LOAD:
			case Menu.CODE_CHOOSE:
			case Menu.CODE_CONFIG:
			case Menu.CODE_MORE:
			case Menu.CODE_CONFIG_SG_ENABLED:
			case Menu.CODE_CONFIG_TIME_ENABLED:
			case Menu.CODE_CONFIG_DIFFICULTY:
				this.navigateItem = 0;
				this.allowSubItems = true;
				this.chooseCode = code;
				break;
			// codes to close menu and run the game
			case Menu.CODE_NEW:
			case Menu.CODE_RESUME:
			case Menu.CODE_RESTART:
				if(code == Menu.CODE_NEW) {
					Game.setLevel(1);
				}
				if(code != Menu.CODE_RESUME) {
					Game.reset(true);
				}
				Game.getState().setStateRunning();
				this.paused = false;
				break;
			// codes with storage action in the game
			case Menu.CODE_SAVE_OPTION_1:
			case Menu.CODE_SAVE_OPTION_2:
			case Menu.CODE_SAVE_OPTION_3:
				if(Game.getStorage().store(code)) {
					this.saveOptions[this.navigateItem] = true;					
				}
				break;
			// codes to load the options of storage in the game
			case Menu.CODE_LOAD_OPTION_1:
			case Menu.CODE_LOAD_OPTION_2:
			case Menu.CODE_LOAD_OPTION_3:
				Game.getStorage().carrer(code);
				break;
			case Menu.CODE_MORE_HOW_PLAY:
			case Menu.CODE_MORE_ABOUT:
			case Menu.CODE_MORE_CREDITS:
				Game.screens.setScreen(code);
				Game.getState().setStateScreens();
				break;
			// Menu - Configuration to enabled the song
			case Menu.CODE_CONFIG_SG_ENABLED_1:
			case Menu.CODE_CONFIG_SG_ENABLED_0:
			case Menu.CODE_CONFIG_SG_ENABLED_2:
			case Menu.CODE_CONFIG_SG_ENABLED_3:
				Game.enabledBGSong = (code == Menu.CODE_CONFIG_SG_ENABLED_1 || code == Menu.CODE_CONFIG_SG_ENABLED_2) ? true : false;
				Game.enabledEffectSong = (code == Menu.CODE_CONFIG_SG_ENABLED_1 || code == Menu.CODE_CONFIG_SG_ENABLED_3) ? true : false;
				this.allowSubItems = false;
				break;
			// Menu - Configuration to enabled the Time
			case Menu.CODE_CONFIG_TIME_ENABLED_1:
			case Menu.CODE_CONFIG_TIME_ENABLED_0:
				Game.hasTime = (code == Menu.CODE_CONFIG_TIME_ENABLED_1 ? true : false);
				this.allowSubItems = false;
				break;
			// Menu - choose phases 
			case Menu.CODE_CHOOSE_PHASE_1:
			case Menu.CODE_CHOOSE_PHASE_2: 
			case Menu.CODE_CHOOSE_PHASE_3:
			case Menu.CODE_CHOOSE_PHASE_4:
			case Menu.CODE_CHOOSE_PHASE_5:
			case Menu.CODE_CHOOSE_PHASE_6:
			case Menu.CODE_CHOOSE_PHASE_7:
			case Menu.CODE_CHOOSE_PHASE_8:
			case Menu.CODE_CHOOSE_PHASE_9:
			case Menu.CODE_CHOOSE_PHASE_10:
				Game.setLevel(Integer.parseInt(code));
				Game.reset(true);
				Game.getState().setStateRunning();
				break;
			case Menu.CODE_CONFIG_DIFFICULTY_1:
			case Menu.CODE_CONFIG_DIFFICULTY_2:
			case Menu.CODE_CONFIG_DIFFICULTY_3:
				int value = Integer.parseInt(code.replace(Menu.CODE_CONFIG_DIFFICULTY +"_", ""));
				Game.setDifficulty(value);
				this.allowSubItems = false;
				break;
		}
	}
	
	private List<String> proccessItems(String label)
	{
		String[] output = new String[Storage.MAXSPACE];
		
		for(int i = 0; i < Storage.MAXSPACE; i++) {
			output[i] = (i+1)+" - "+(this.saveOptions[i] ? label : "- - - - - -");
		}
		return Arrays.asList(output);
	}
	
	public Menu()
	{
		String[] array = {CODE_SAVE_OPTION_1, CODE_SAVE_OPTION_2,CODE_SAVE_OPTION_3};
		for(int i = 0; i < array.length;i++) {
			this.saveOptions[i] = Game.getStorage().fileExist(array[i]);
		}
	}
	
	public void tick()
	{
		this.maxItems = (this.generageItems().size() - 1);
		
		if(this.up) {
			this.up = false;
			this.navigateItem--;
			if(this.navigateItem < 0) {
				this.navigateItem = this.maxItems;
			}
		}
		else if(this.down) {
			this.down = false;
			this.navigateItem++;
			if(this.navigateItem > this.maxItems) {
				this.navigateItem = 0;
			}
		}
		else if(this.choosed)
		{
			this.choosed = false;
			this.chooseItem();
		}
		else if(this.deleting)
		{
			String code = this.itemCodes.get(this.navigateItem);
			this.deleting = false;
			
			switch(code){
				case Menu.CODE_LOAD_OPTION_1:
				case Menu.CODE_LOAD_OPTION_2:
				case Menu.CODE_LOAD_OPTION_3:
					Game.getStorage().detech(code);
					this.saveOptions[this.navigateItem] = false; 
					Animation.setFlashMessage("removendo arquivo: "+code, Animation.FLASH_INFO);
					break;
			}
		}
	}
	
	public void render(Graphics g)
	{
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		double mdl = 2.5;
		
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, xxx, yyy);
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_10));
		g2.setColor(new Color(77,166,255));
		
		g2.drawString(Game.NAME, (int)(xxx / mdl), (yyy /8));
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_6));
		g2.setColor(new Color(224, 205,137));
		g2.drawString("Difficulty: "+Game.getDifficultyLabel(), (int)(xxx - Game.SCALE_55), Game.SCALE_40);
		g2.drawString("Phase: "+Game.getLevel(), (int)(xxx - Game.SCALE_55), Game.SCALE_50);
		
		if(Game.isInfiniteEnergy()) {
			g2.setColor(Color.ORANGE);
			g2.drawString("Infinite energy", (int)(xxx - Game.SCALE_55), Game.SCALE_50+Game.SCALE_10);
		}
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_8));
		g2.setColor(Color.WHITE);
		
		int increaseItem = Game.SCALE_12;
		int initiaPosItem = (yyy/4);
		
		ArrayList<String> items = this.generageItems();
		
		for(int o = 0; o < items.size();o++) {
			String label = items.get(o);
			g2.drawString(label, Game.SCALE_10, initiaPosItem + (increaseItem * o));
		}
		g2.drawString(">", Game.SCALE_4, initiaPosItem + (increaseItem * this.navigateItem) );
		
		if(this.chooseCode == Menu.CODE_LOAD) {
			String msg = "Delete a storage space pressing X key in the index wished";
			g2.drawString(msg, Game.SCALE_10, (yyy-Game.SCALE_30));
		}
	}
	
	public void initb(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(new Color(0,0,0, this.timeropacity));
		g2.fillRect(0, 0, Game.getXcale(), Game.getYcale());
		
		this.timeropacity--;
		
		if(this.timeropacity < 55)
			this.timeropacity = 255;
		
		if(Game.validframes) {
			Game.getState().setStateMenu();
		}
		g2.drawString("bug", 0, 0);
	}
	
	public void setAllowSubItems(boolean allowSubItems)
	{
		this.chooseCode = null;
		this.navigateItem = 0;
		this.allowSubItems = allowSubItems;
	}
	
	public void setUp(boolean up) {
		this.up = up;
	}

	public void setDown(boolean down) {
		this.down = down;
	}
	public void setRight(boolean right) {
		this.right = right;
		this.left = !right;
	}
	
	public boolean getRight() {
		return this.right;
	}

	public void setLeft(boolean left) {
		this.left = left;
		this.right = !left;
	}
	
	public boolean getLeft() {
		return this.left;
	}
	
	public void setChoosed(boolean choosed) {
		this.choosed = choosed;
	}
	
	public void setPaused(boolean paused) {
		this.paused = paused;
	}
	
	public void setDeleting(boolean deleting) {
		this.deleting = deleting;
	}
	public void setChooseLevel() {
		this.chooseCode = Menu.CODE_CHOOSE;
		this.allowSubItems = true;
	}
}

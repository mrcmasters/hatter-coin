package com.mrcardoso.graphcs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;

public class Animation {
	
	public static final int FLASH_INFO = 1; 
	public static final int FLASH_SUCCESS = 2; 
	public static final int FLASH_WARNING = 3; 
	public static final int FLASH_ERROR = 4; 
	private static int flashFrames = 0, flashMaxFrames = 80;
	private static String flashMessage = ""; 
	private static int flashType = FLASH_INFO;
	private static int animateIndex = 0,animateFrames = 0, animateMaxFrames = 30;
	private static int typingIndex = 0, typingFrames = 0, typingMaxFrames = 12, typingTextIndex = 0;
	
	public static void resetTyping() {
		typingIndex = 0;
		typingFrames = 0;
		typingTextIndex = 0;
	}
	
	public static void setFlashMessage(String flashMessage, int flashType)
	{
		Animation.flashType = flashType;
		Animation.flashMessage = flashMessage;
	}
	
	public static void typing(Graphics g, String[] message, int xx, int yy, Color color) {
		
		typingFrames++;
		if(typingFrames==typingMaxFrames) {
			typingFrames=0;
			
			if(typingIndex < message[typingTextIndex].length()){
				typingIndex++;
			} else {
				typingIndex = 0;
				if(typingTextIndex < (message.length-1))
					typingTextIndex++;
				else
					typingTextIndex = 0;
			}
		}
		g.setFont(new Font("arial", Font.BOLD, Game.SCALE_10));
		g.setColor(color);
		g.drawString(message[typingTextIndex].substring(0, typingIndex), xx, yy);
	}
	
	public static void blink(Graphics g, String message, int xx, int yy, Color color)
	{
		animateIndex++;
		if(animateIndex < animateMaxFrames)
		{
			g.setFont(new Font("arial", Font.BOLD, Game.SCALE_10));
			g.setColor(color);
			g.drawString(message, xx, yy);
		}
		else
		{
			animateFrames++;
			if(animateFrames==animateMaxFrames) {
				animateIndex = 0;
				animateFrames=0;
			}
		}
	}
	
	public static void flashInfo(Graphics g, int xx, int yy) {
		Animation.flash(g, xx, yy, new Color(110,220,255), new Color(58,135,173));
	}
	public static void flashSuccess(Graphics g, int xx, int yy) {
		Animation.flash(g, xx, yy, new Color(214,233,198), new Color(70,136,71));
	}
	public static void flashWarning(Graphics g, int xx, int yy) {
		Animation.flash(g, xx, yy, new Color(250,234,204), new Color(186,146,73));
	}
	public static void flashError(Graphics g, int xx, int yy) {
		Animation.flash(g, xx, yy, new Color(238,211,215), new Color(185,74,72));
	}
	
	public static void flashBlock(Graphics g, int xx, int yy)
	{
		switch(Animation.flashType) {
			case FLASH_INFO: Animation.flashInfo(g, xx, yy); break;
			case FLASH_SUCCESS: Animation.flashSuccess(g, xx, yy); break;
			case FLASH_WARNING: Animation.flashWarning(g, xx, yy);break;
			case FLASH_ERROR: Animation.flashError(g, xx, yy); break;
		}
	}
	
	private static void flash(Graphics g, int xx, int yy, Color color, Color border)
	{
		if(!Animation.flashMessage.isEmpty())
		{
			if(Animation.flashFrames < Animation.flashMaxFrames)
			{
				Animation.flashFrames++;
				g.setColor(color);
				g.fillRect(Game.SCALE_4,yy-Game.SCALE_10, Game.getXcale()-Game.SCALE_8,Game.SCALE_12);
				g.setColor(border);
				g.drawRect(Game.SCALE_4,yy-Game.SCALE_10, Game.getXcale()-Game.SCALE_8,Game.SCALE_12);
				g.setColor(border);
				g.drawString(Animation.flashMessage, xx-Animation.flashMessage.length(),yy-Game.SCALE_2);
			} else {
				Animation.flashFrames = 0;
				Animation.flashMessage = "";
			}
		}
	}

	@SuppressWarnings("unused")
	private static void rotation(Graphics g)
	{
		int middleScreenX = (Game.getXcale()/3) + (Game.WIDTH/3);
		int middleScreenY = (Game.getYcale()-Game.SCALE_50);
		int middleX = middleScreenX+Game.SCALE_10;
		int middleY = middleScreenY+Game.SCALE_10;
		double radians = Math.atan2(middleY -  Game.getMouseY(), middleX - Game.getMouseX());
		
		Graphics2D g2 = (Graphics2D) g;
		g2.rotate(radians, middleX, middleY);
		g2.setColor(Color.RED);
		g2.fillRect(middleScreenX, middleScreenY, Game.SCALE_20, Game.SCALE_20);
	}
}

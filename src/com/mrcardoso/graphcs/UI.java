package com.mrcardoso.graphcs;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import com.mrcardoso.core.Game;
import com.mrcardoso.core.World;
import com.mrcardoso.entities.Player;
import com.mrcardoso.helpers.TimerWatch;

public class UI {
	
	public void render(Graphics g)
	{
		Player p = Game.player;
		int xxx = Game.getXcale();
		int yyy = Game.getYcale();
		int energy = p.getEnergy();
		String textEnergy = Game.isInfiniteEnergy() ? "--" : Integer.toString(energy);
		int energyBoxSize = Game.SCALE_50;
		
		int initialPosition = (int) (xxx/1.3);
		int initialPositionY =  Game.SCALE_14;
		int[] entityItems = {World.HEX_COIN, World.HEX_LIFE, World.HEX_ENEMY};
		
		g.setColor(Color.RED);
		g.fillRect(Game.SCALE_4, Game.SCALE_8, Game.SCALE_50, Game.SCALE_8);
		
		g.setColor(new Color(255,229,153));
		g.fillRect(Game.SCALE_4, Game.SCALE_8, (int) ( (p.getLife()/p.getMaxLife()) * Game.SCALE_50), Game.SCALE_8);
		
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.BOLD, Game.SCALE_6));
		g.drawString((int)(p.getLife()) +"/"+(int)p.getMaxLife(), Game.SCALE_10, Game.SCALE_14);
		
		if(Game.hasTime)
		{
			g.setFont(new Font("arial", Font.BOLD, Game.SCALE_20));
			g.drawString(Integer.toString(TimerWatch.getTimeLimit()), (int)(xxx/2.2), Game.SCALE_24);			
		}
		
		if(Game.isLastLevel() && Game.levelup.isAllowed())
		{
			Animation.blink(g, "Game Ending!!!", xxx/3, yyy-10, Color.ORANGE);
		}
		
		Graphics2D g2 = (Graphics2D) g;
		
		g2.setFont(new Font("arial", Font.BOLD, Game.SCALE_6));
		g2.setColor(new Color(123,57,157));
		g2.fillRect(Game.SCALE_4, Game.SCALE_16,  energyBoxSize, Game.SCALE_8);
		g2.setColor(Color.WHITE);
		g2.drawString(textEnergy, Game.SCALE_8, Game.SCALE_22);
		
		if(Game.isEasyMode()) {
			g2.setColor(new Color(0,0,0, 140));
			g2.fillRect(initialPosition, Game.SCALE_6, Game.SCALE_50, Game.SCALE_28);
			
			for(int index = 0; index <  entityItems.length;index++)
			{
				int value = 0;
				String label = "";
				switch(entityItems[index]) {
					case World.HEX_ENEMY:
						value = p.getEnemyTotal();
						label = "Enemy";
						break;
					case World.HEX_LIFE:
						value = p.getLifeTotal();
						label = "Life";
						break;
					case World.HEX_COIN:
						value = p.getCoinTotal();
						label = "Coin";
						break;
				}
				
				String text = label+": "+Integer.toString(value); 
				
				g2.setColor(Color.ORANGE);
				g2.drawString(text, initialPosition + Game.SCALE_6, initialPositionY);
				initialPositionY += Game.SCALE_8;
			}
		}
	}
}

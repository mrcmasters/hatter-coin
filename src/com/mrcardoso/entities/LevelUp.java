package com.mrcardoso.entities;

import java.awt.Graphics;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;

public class LevelUp extends Entity{
	
	private boolean isAllowed = false;
	public LevelUp() {
		super(Game.OZ, Game.OZ, Game.OZ, Game.OZ, Entity.LEVEUP);
	}
	
	public void render(Graphics g)
	{
		if(this.isAllowed)
		{
			g.drawImage(this.sprite, Camera.getRealX(this.getX()), Camera.getRealY(this.getY()), null);
		}
	}

	public boolean isAllowed() {
		return this.isAllowed;
	}
	public void setAllowed(boolean isAllowed) {
		this.isAllowed = isAllowed;
	}

}
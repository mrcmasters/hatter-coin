package com.mrcardoso.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;
import com.mrcardoso.core.World;
import com.mrcardoso.movement.Movement;
import com.mrcardoso.movement.WalkDirectionMovement;
import com.mrcardoso.sound.Sound;

public class Player extends Entity implements WalkDirectionMovement
{
	private static final double SPEED = 0.9;
	private static final double MAX_SPEED = 1.5;
	private boolean isAttacking = false, mouseAttack = false;
	private double life = 100;
	private int maxLife = 100;
	private int accumulatedCoins = 0;
	private int accumulatedEnemy = 0;
	private int energy = 0;
	private int maxEnergy = 10;
	private int mouseX, mouseY;
	private int energyTotal = 0, lifeTotal = 0,enemyTotal = 0, coinTotal = 0;
	private int enemySize = 0;

	private static final BufferedImage[] SPRITE_JUMP = {
			Game.spritesheet.getSprite(0, 0, Game.OZ, Game.OZ),
			Game.spritesheet.getSprite(Game.OZ * 1, 0, Game.OZ, Game.OZ),
			Game.spritesheet.getSprite(Game.OZ * 2, 0, Game.OZ, Game.OZ),
			Game.spritesheet.getSprite(Game.OZ * 3, 0, Game.OZ, Game.OZ)	
	};
	private static final BufferedImage[] SPRITE_RIGHT = {
		Game.spritesheet.getSprite( ( Game.OZ * 4), 0, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite( ( Game.OZ * 5 ), 0, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite( ( Game.OZ * 6 ), 0, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite( ( Game.OZ * 7 ), 0, Game.OZ, Game.OZ)
	};
	private static final BufferedImage[] SPRITE_LEFT = {
		Game.spritesheet.getSprite( ( Game.OZ * 8), 0, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite( ( Game.OZ * 9 ), 0, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite( ( Game.OZ * 10 ), 0, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite( ( Game.OZ * 11 ), 0, Game.OZ, Game.OZ)
	};
	private static final BufferedImage[] SPRITE_IDLE = {
		Game.spritesheet.getSprite(Game.OZ * 8, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 9, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 10, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 11, Game.OZ, Game.OZ, Game.OZ)
	};
	private static BufferedImage[] SPRITE_LOSE;
	private List<Projectile> energies;
	
	protected boolean right, left, up, down, jump, isJumping, isFalling = true, inFloor = false, isRightSide = true;
	public double speed = Player.SPEED;
	
	public Player() {
		super(0, 0, Game.OZ, Game.OZ, null);
		
		this.frames = 0;
		this.index = 0;
		this.maxFrames = 10;
		this.maxIndex = 3;
		this.accumulatedCoins = 0;
		this.accumulatedEnemy = 0;
		
		if(Game.isLastLevel()) {
			this.maxEnergy = (Game.isHardMode() ? 100 : Game.isNormalMode() ? 150 : 200);
		} else {
			this.maxEnergy = (Game.isHardMode() ? 40 : Game.isNormalMode() ? 50 : 80);
		}
		
		this.energies =  new ArrayList<Projectile>();
	}
	
	public void tick()
	{
		if(this.life <= 0 || this.loseByFallHole() )
		{
			Game.lose();
			return;
		}
		
		double dx = (this.isJumpingActive() ? 0 : (this.isRightSide ? 1 : -1));
		double dy = (this.isJumpingActive() ? 1 : 0);
		
		int px = (this.isJumpingActive() ? 6 : (this.isRightSide ? 9 : 3));
		int py = (this.isJumpingActive() ? 15 : (this.isRightSide ? 4 : 3));
		
		Movement.directionX(this);
		
		this.attackWithKeyCode(dx, dy, px, py);
		
		// ignore for this first build
		//if(!Game.isFullScreen) {			
		//	this.attackWithMouse(px, py);
		//}
		
		for(int i = 0; i< this.energies.size(); i++)
		{
			 this.energies.get(i).tick();
		}
		
		this.checkCollission();
		this.updateCamera();
		
		if(this.isMoving()){
			this.walk();
		} else {
			this.speed = Player.SPEED;
		}
	}

	public void render(Graphics g)
	{
		int rx = Camera.getRealX(this.getX());
		int ry = Camera.getRealY(this.getY());
		
		if(this.isJumpingActive()) {
			this.animateJump();
			g.drawImage(Player.SPRITE_JUMP[this.indexJump], rx, ry, null);
		} else if(this.right) {
			g.drawImage(Player.SPRITE_RIGHT[this.index], rx, ry, null);			
		} else if(this.left) {
			g.drawImage(Player.SPRITE_LEFT[this.index], rx, ry, null);
		} else {
			g.drawImage(Player.SPRITE_IDLE[this.isRightSide? 0 : 2], rx, ry, null);
		}
		
		for(int i = 0; i< this.energies.size(); i++)
		{
			 this.energies.get(i).render(g);
		}
	}
	
	protected void checkCollission()
	{
		for(int i = 0; i< Game.entities.size(); i++)
		{
			Entity current = Game.entities.get(i);
			if(this.isColliding(current))
			{	
				if(current instanceof Boss) {
					this.reduceLife(Boss.getDamage());
				} 
				else if(current instanceof Enemy)
				{
					Enemy eny = ((Enemy) current);
					eny.setIsMoving(this.isMoving());
					if(Game.HAS_GRAVITY && isFalling) {
						eny.reduceLife((double)eny.getMaxLife()/2);
						this.startJump(false);
					} else {
						this.reduceLife(Enemy.getDamage());
					}
				}
				else if(current instanceof LifePack)
				{
					if(this.updateLife()) {
						this.decreaseLifeTotal();
						Game.entities.remove(current);
					}
				}
				else if(current instanceof Energy)
				{
					if(this.updateEnergy()) {
						this.decreaseEnergyTotal();
						Game.entities.remove(current);
					}
				}
				else if(current instanceof Coin)
				{
					current.destroyAnimated();
				}
				else if(current instanceof Elevation) {
					if(this.nextY() > current.getY()) {
						this.startJump(false);
					}
				}
			}
		}
	}
	
	protected boolean updateLife() {
		boolean hasLife = (this.life == this.maxLife ? false : true);
		this.life += Game.rand.nextInt(20) + 10;
		if(this.life >= this.maxLife) {
			this.life = this.maxLife;
		}
		return hasLife;
	}
	
	protected void reduceLife(int damage) {
		if(Game.rand.nextInt(100) < (20 * Game.getDifficulty()) ) {
			this.setLife(this.getLife()-(Game.rand.nextInt(Game.getDifficulty() * damage)+1));
			World.gererateParticles(1, this.getX()+8, this.getY()+8, Color.YELLOW);
		}
	}
	
	protected boolean updateEnergy()
	{
		int value = (Game.isHardMode() ? 20 :Game.isNormalMode() ? 25 : 40);
		
		if((this.energy+value) <= this.maxEnergy) {
			this.energy += value;
			return true;
		}
		
		return false;
	}
	
	protected boolean isMoving()
	{
		return (this.right || this.left);
	}
	
	protected boolean isJumpingActive() {
		return this.jumpFromKey && this.jump;
	}
	
	public void updateCamera()
	{
		Camera.x = Camera.clamp((this.getX() - (Game.WIDTH/2)), 0, (World.WIDTH*Game.OZ) - Game.WIDTH);
		Camera.y = Camera.clamp( (this.getY() - (Game.HEIGHT/2)), 0, (World.HEIGHT*Game.OZ) - Game.HEIGHT);
	}
	
	protected void attackWithMouse(int px, int py)
	{
		if(this.mouseAttack)
		{
			this.mouseAttack = false;
			if(this.canShootEnergy())
			{
				double angle = Math.atan2(this.mouseY - Camera.getRealY(this.getY()+px), this.mouseX - Camera.getRealX(this.getX()+px));
				double dx = Math.cos(angle);
				double dy = Math.sin(angle);
				
				this.attachEnergies(new Projectile(this.getX() + px, this.getY() + py, 4, 3, dx, dy));
			}
		}
	}
	
	protected void attackWithKeyCode(double dx, double dy, int px, int py)
	{
		if(this.isAttacking)
		{
			this.isAttacking  = false;
			if(this.canShootEnergy())
			{
				Sound.shoot.play();
				this.attachEnergies(new Projectile(this.getX() + px, this.getY() + py, 3, 3, dx, dy));
			}
		}
	}
	
	public static void renderSpriteLose() {
		if(Player.SPRITE_LOSE == null) {
			Player.SPRITE_LOSE = new BufferedImage[4];
			Player.SPRITE_LOSE[0]  = Game.spritesheet.getSprite(Game.OZ * 12, 0, Game.OZ, Game.OZ);
			Player.SPRITE_LOSE[1]  = Game.spritesheet.getSprite(Game.OZ * 13, 0, Game.OZ, Game.OZ);
			Player.SPRITE_LOSE[2]  = Game.spritesheet.getSprite(Game.OZ * 12, Game.OZ, Game.OZ, Game.OZ);
			Player.SPRITE_LOSE[3]  = Game.spritesheet.getSprite(Game.OZ * 13, Game.OZ, Game.OZ, Game.OZ);
		}
	}
	public static BufferedImage getSpriteLose(int index) {
		if(index < 0 || index > (Player.SPRITE_LOSE.length-1) ) {
			return null;
		}
		
		return Player.SPRITE_LOSE[index];
	}
	
	public static int spriteLoseSize() {
		return Player.SPRITE_LOSE.length;
	}
	
	public static BufferedImage getJumpSprite(int index) {
		BufferedImage[] spritesJump = Player.SPRITE_JUMP;
		if(index < 0 || index > (spritesJump.length-1) ) {
			return null;
		}
			
		return Player.SPRITE_JUMP[index];
	}
	
	public static int jumpSpriteSize() {
		return Player.SPRITE_JUMP.length;
	}
	
	
	// getters setters
	
	public void refrashAllowLevelUp() {
		boolean rule = (Game.isLastLevel() ? (this.enemyTotal == 0) : true);
		Game.levelup.setAllowed(rule);
	}
	
	public void startJump(boolean fromKey) {
		this.jump = true;
		this.isJumping = true;
		this.isFalling = false;
		this.inFloor = false;
		this.setJumpFromKey(fromKey);
	}
	
	public boolean canShootEnergy() {
		return Game.isInfiniteEnergy() || this.energy > 0;
	}
	
	public boolean isUp() {
		return this.up;
	}

	public void setUp(boolean up) {
		this.up = up;
	}

	public boolean isDown() {
		return this.down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public boolean isRight() {
		return this.right;
	}
	
	public void setRight(boolean right) {
		this.right = right;
	}

	public boolean isLeft() {
		return this.left;
	}
	public void setLeft(boolean left) {
		this.left = left;
	}

	public boolean isJump() {
		return this.jump;
	}

	public void setJump(boolean jump) {
		this.jump = jump;
	}

	public boolean isJumping() {
		return this.isJumping;
	}

	public void setJumping(boolean isJumping) {
		this.isJumping = isJumping;
	}

	public boolean isFalling() {
		return isFalling;
	}

	public void setFalling(boolean isFalling) {
		this.isFalling = isFalling;
	}

	public void setRightSide(boolean isRightSide) {
		this.left = !isRightSide;
		this.right = isRightSide;
		this.isRightSide = isRightSide;
	}
	
	public boolean isInFloor() {
		return this.inFloor;
	}

	public void setInFloor(boolean inFloor) {
		this.inFloor = inFloor;
	}

	public int getEnergyTotal() {
		return this.energyTotal;
	}
	
	public void increaseEnergyTotal() {
		this.energyTotal++;
	}
	
	public void decreaseEnergyTotal(){
		if(this.energyTotal>0) this.energyTotal--;
	}
	
	public int getAccumulatedCoins() {
		return this.accumulatedCoins;
	}
	
	public void incrementAccumulatedCoins() {
		this.accumulatedCoins++;
	}
	
	public int getAccumulatedEnemy() {
		return this.accumulatedEnemy;
	}
	
	public void incrementAccumulatedEnemy() {
		this.accumulatedEnemy++;
	}
	
	public int getCoinTotal() {
		return this.coinTotal;
	}
	
	public void increaseCoinTotal() {
		this.coinTotal++;
	}
	
	public void decreaseCoinTotal() {
		if(this.coinTotal>0) this.coinTotal--;
	}
	
	public int getLifeTotal() {
		return this.lifeTotal;
	}
	
	public void increaseLifeTotal() {
		this.lifeTotal++;
	}
	
	public void decreaseLifeTotal(){
		if(this.lifeTotal>0) this.lifeTotal--;
	}
	
	public int getEnemyTotal() {
		return this.enemyTotal;
	}
	
	public void increaseEnemyTotal() {
		this.enemyTotal++;
	}
	
	public void initialSizes() {
		this.enemySize = this.enemyTotal;
	}
	
	public int getEnemySize() {
		return this.enemySize;
	}
	
	public void decreaseEnemyTotal(){
		if(this.enemyTotal>0) {
			this.enemyTotal--;
		}	
		this.refrashAllowLevelUp();
	}

	public void setLife(double l) {
		this.life = l;
	}
	
	public double getLife()
	{
		return this.life;
	}
	
	public double getMaxLife()
	{
		return this.maxLife;
	}
	
	public int getEnergy() {
		return this.energy;
	}
	
	public void setEnergy(int energy) {
		this.energy = energy;
	}
	
	public boolean getIsAttacking() {
		return this.isAttacking;
	}
	
	public void setIsAttacking(boolean isAttacking) {
		this.isAttacking = isAttacking;
	}
	
	public boolean isMouseAttack() {
		return this.mouseAttack;
	}
	
	public void setMouseAttack(int x, int y) {
		this.mouseAttack = true;
		this.mouseX = x;
		this.mouseY = y;
	}
	
	public void detachEnergies(Projectile attack) {
		this.energies.remove(attack);
	}
	
	public void attachEnergies(Projectile attack)
	{
		if(this.canShootEnergy())
		{
			if(this.energy > 0)
				this.energy--;
			this.energies.add(attack);		
		}
	}
	
	public List<Projectile> getEnergies() {
		return this.energies;
	}
	
	@Override
	public double nextX()
	{
		return this.x + this.speed;
	}
	@Override
	public double nextY()
	{
		return this.y + (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}
	@Override
	public double prevX()
	{
		return this.x - this.speed;
	}
	@Override
	public double prevY()
	{
		return this.y - (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}
	@Override
	public void increaseX()
	{
		this.x += this.speed;
		this.updateSpeed();
	}
	@Override
	public void decreaseX()
	{
		this.x -= this.speed;
		this.updateSpeed();
	}
	@Override
	public void increaseY()
	{
		this.y += (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}
	@Override
	public void decreaseY()
	{
		this.y -= (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}

	@Override
	public void updateSpeed() {
		if(this.speed < MAX_SPEED && !this.isFalling) {
			this.speed+=0.5;
		}
	}
}

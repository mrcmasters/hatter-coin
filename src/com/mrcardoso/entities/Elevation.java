package com.mrcardoso.entities;

import com.mrcardoso.core.Game;

public class Elevation extends Entity {
	protected double speed = 0.8;
	protected boolean up = true, down = false;

	public Elevation(int x, int y) {
		super(x, y, Game.OZ, Game.OZ, Entity.ELEVATION);
	}
}

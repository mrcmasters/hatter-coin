package com.mrcardoso.entities;

import com.mrcardoso.core.Game;

public class LifePack extends Entity{
		
	public LifePack(int x, int y) {
		super(x, y, Game.OZ, Game.OZ, Entity.LIFEPACK);
		
		this.maskX = 4;
		this.maskY = 4;
		this.maskWidth = 8;
		this.maskHeight = 8; 
	}

}

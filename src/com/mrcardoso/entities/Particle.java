package com.mrcardoso.entities;

import java.awt.Color;
import java.awt.Graphics;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;

public class Particle extends Entity{
	private int life = 10;
	private int lifecycle = 0;
	private double speed = 1.6;
	private double dx = 0;
	private double dy = 0;
	private Color color = Color.RED;
	

	public Particle(int x, int y, int width, int height, Color color) {
		super(x, y, width, height, null);
		this.color = color;
		
		this.dx = Game.rand.nextGaussian();
		this.dy = Game.rand.nextGaussian();
	}
	
	public void tick() {
		this.x += this.dx*this.speed;
		this.y += this.dy*this.speed;
		
		this.lifecycle++;
		if(this.lifecycle==this.life) {
			Game.entities.remove(this);
		}
	}
	
	public void render(Graphics g)
	{
		g.setColor(this.color);
		g.fillRect(Camera.getRealX(this.getX()), Camera.getRealY(this.getY()), this.width,this.height);
	}

}

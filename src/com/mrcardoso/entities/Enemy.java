package com.mrcardoso.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.List;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;
//import com.mrcardoso.core.MiniMap;
import com.mrcardoso.core.World;
import com.mrcardoso.ia.EngineIA;
import com.mrcardoso.ia.Node;
import com.mrcardoso.movement.WalkDirectionMovement;
import com.mrcardoso.sound.Sound;

public class Enemy extends Entity implements WalkDirectionMovement

{
	protected boolean isMoving = true;
	protected double life = 10;
	protected int maxLife = 10;
	
	protected static final BufferedImage[] SPRITE_RIGHT = {
		Game.spritesheet.getSprite(Game.OZ * 2, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 3, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 4, Game.OZ, Game.OZ, Game.OZ)
	};
	protected static final BufferedImage[] SPRITE_LEFT = {
		Game.spritesheet.getSprite(Game.OZ * 5, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 6, Game.OZ, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ * 7, Game.OZ, Game.OZ, Game.OZ)	
	};
	protected EngineIA ia;
	protected List<Node> path;
	
	public double speed = 0.8, variation = 0;
	public boolean right = true, left = false;
	public static int getDamage() { return 4; }

	public Enemy(int x, int y) {
		super(x, y, Game.OZ, Game.OZ, null);
		this.variation = 0 + Game.rand.nextDouble() * (0.4 - 0);
		this.speed += this.variation;
		
		this.frames = 0;
		this.index = 0;
		this.maxFrames = 10;
		this.maxIndex = 2;
		
		this.ia = new EngineIA(this, EngineIA.FOLLOW_RIGHT_LEFT);
		this.setMask(4, 4, 8, 8);
	}
	
	public void tick()
	{
		Player p = Game.player;
		
		if(this.life <= 0 || this.loseByFallHole()) {
			this.destroy();
			return;
		}
		
		if(this.distanceBetweeen(this.getX(), this.getY(), p.getX(), p.getY()) < (Game.WIDTH/1) ){
			this.ia.tick();
			this.walk();
		}
		this.collidingWithEnergy();
	}
	
	public void render(Graphics g)
	{
		int rx = Camera.getRealX(this.getX());
		int ry = Camera.getRealY(this.getY());
		Player p = Game.player;
		
		boolean onlyWalk = (this.ia.getMode() == EngineIA.FOLLOW_RIGHT_LEFT);
		boolean onLeft = false, onRight = false;
		
		if(onlyWalk) {
			onLeft = this.left;
			onRight = this.right;
		} else {
			onRight = (this.getX() < p.getX());
			onLeft = (this.getX() > p.getX());
		}
		
		if(onRight) {
			g.drawImage(Enemy.SPRITE_RIGHT[this.index], rx, ry, null);			
		} else if(onLeft) {
			g.drawImage(Enemy.SPRITE_LEFT[this.index], rx, ry, null);
		} else {
			g.drawImage(Enemy.SPRITE_LEFT[0], rx, ry, null);
		}
		
		g.setColor(new Color(255,0,0));
		g.fillRect(Camera.getRealX(this.getXMask()), ry, (int) ((this.life/this.maxLife)*7), 2);
		
		if(this.debugMask)
		{
			g.setColor(Color.BLUE);
			g.fillRect(Camera.getRealX(this.getXMask()), Camera.getRealY(this.getYMask()), this.maskWidth, this.maskHeight);			
		}
	}
	
	public void setIsMoving(boolean isMoving)
	{
		this.isMoving = isMoving;
	}
	
	public boolean getIsMoving()
	{
		return this.isMoving;
	}
	
	public List<Node> getPath() {
		return this.path;
	}

	public void setPath(List<Node> path) {
		this.path = path;
	}
	
	public void reduceLife(double life)
	{
		this.life -= life;
	}
	
	public double getMaxLife()
	{
		return this.maxLife;
	}
	
	public boolean isColliding(int xnext, int ynext)
	{
		Rectangle enemyCurrent = new Rectangle(xnext+this.maskX, ynext+this.maskY, this.maskWidth, this.maskHeight);
		for(int i = 0; i< Game.enemies.size(); i++)
		{
			Enemy e = Game.enemies.get(i);
			if(e == this) {
				continue;
			}
			Rectangle enemyTarget = new Rectangle(e.getXMask(), e.getYMask(), this.maskWidth, this.maskHeight);
			if(enemyTarget.intersects(enemyCurrent)) {
				return true;
			}
		}
		return false;
	}
	
	protected void collidingWithEnergy()
	{
		Player p = Game.player;
		
		for(int i = 0; i< p.getEnergies().size(); i++) {
			Projectile ea = p.getEnergies().get(i);
			if(this.isColliding(ea)) {
				this.reduceLife(Game.rand.nextInt(5) + 1);
				p.detachEnergies(ea);
				
				if(this.life <= 0){
					break;
				}
			}
		}
	}

	protected void destroy() {
		if(this.life<=0) {
			World.gererateParticles(60, this.getX(), this.getY(), Color.RED);
			Game.player.incrementAccumulatedEnemy();
			Sound.explosion.play();
		}
		Game.enemies.remove(this);
		Game.entities.remove(this);
		Game.player.decreaseEnemyTotal();
	}
	
	@Override
	public double nextX()
	{
		return this.x + this.speed;
	}
	@Override
	public double nextY()
	{
		return this.y + (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}
	@Override
	public double prevX()
	{
		return this.x - this.speed;
	}
	@Override
	public double prevY()
	{
		return this.y - (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}
	@Override
	public void increaseX()
	{
		this.x += this.speed;
	}
	@Override
	public void decreaseX()
	{
		this.x -= this.speed;
	}
	@Override
	public void increaseY()
	{
		this.y += (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}
	@Override
	public void decreaseY()
	{
		this.y -= (Game.HAS_GRAVITY ? Game.GRAVITY : this.speed);
	}

	@Override
	public void updateSpeed() {}
}
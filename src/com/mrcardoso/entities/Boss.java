package com.mrcardoso.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;
import com.mrcardoso.core.World;

public class Boss extends Enemy{
	
	public static final BufferedImage[] SPRITES = {
		Game.spritesheet.getSprite(0, Game.OZ*4, Game.OZ*5, Game.OZ*4),
		Game.spritesheet.getSprite(Game.OZ*5, Game.OZ*4, Game.OZ*5, Game.OZ*4)
	};
	private static int XPAWN = (World.WIDTH*Game.OZ);
	public static int getDamage() { return 8; }
	
	public Boss(int x, int y) {
		super(x, y);
		this.maxIndex = SPRITES.length-1;
		this.maxFrames = 30;
		this.life = 200;
		this.speed = (Game.isHardMode() ? 4 : Game.isNormalMode() ? 2 : 1);
		this.setMask(4, 4, (int) (Game.OZ*4), (int) (Game.OZ*3.5));
		
		this.x = Boss.XPAWN;
		this.y = (World.HEIGHT*Game.OZ)-(Game.OZ*4);
	}
	
	public void tick() {
		if(this.life <= 0) {
			this.destroy();
			return;
		}
		if((this.nextX()+this.maskWidth) < 0) {
			this.x = Boss.XPAWN;
		}
		this.decreaseX();
		this.walk();
		this.collidingWithEnergy();
	}
	
	public void render(Graphics g) {
		int rx = Camera.getRealX(this.getX());
		int ry = Camera.getRealY(this.getY());
		
		g.drawImage(Boss.SPRITES[this.index], rx, ry, null);
	}
}

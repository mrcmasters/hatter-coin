package com.mrcardoso.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;
import com.mrcardoso.sound.Sound;

public class Coin extends Entity
{
	private boolean spritesDestroys = false; 
	private int destroyFrames = 0,destroyMaxFrames = 30;
	public static BufferedImage[] ANIMATES = {
		Game.spritesheet.getSprite(0, Game.OZ*3, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ, Game.OZ*3, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ*2, Game.OZ*3, Game.OZ, Game.OZ)
	};
	
	public static BufferedImage[] DESTROYEDS = {
		Game.spritesheet.getSprite(Game.OZ*3, Game.OZ*3, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ*4, Game.OZ*3, Game.OZ, Game.OZ),
		Game.spritesheet.getSprite(Game.OZ*5, Game.OZ*3, Game.OZ, Game.OZ)
	};
	
	public Coin(int x, int y) {
		super(x, y, Game.OZ, Game.OZ, null);
		this.maxIndex = 2;
	}
	
	protected void destroyAnimated(){
		this.spritesDestroys = true;
	}
	
	public void tick()
	{
		this.walk();
		if(this.spritesDestroys) {
			this.destroyFrames++;
			if(this.destroyFrames == this.destroyMaxFrames) {
				Game.player.decreaseCoinTotal();
				Game.entities.remove(this);
				Game.player.incrementAccumulatedCoins();
				Sound.coin.play();
			}
		}
	}
	
	public void render(Graphics g) {
		int rx = Camera.getRealX(this.getX());
		int ry = Camera.getRealY(this.getY());
		
		if(this.spritesDestroys) {
			g.drawImage(DESTROYEDS[this.index], rx, ry, null);			
		} else {
			g.drawImage(ANIMATES[this.index], rx, ry, null);			
		}
	}
}
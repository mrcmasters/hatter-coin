package com.mrcardoso.entities;

import java.awt.Color;
import java.awt.Graphics;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;

public class Projectile extends Entity{
	
	private double directionX;
	private double directionY;
	private double speed = 1.9;
	private int quantity = 0;
	private int life = 120;
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Projectile(int x, int y, int width, int height, double directionX, double directionY) {
		super(x, y, width, height, null);
		
		this.directionX = directionX;
		this.directionY = directionY;
		this.setMask(1, 1, width, height);
	}

	public void tick()
	{
		this.x += this.directionX*this.speed;
		this.y += this.directionY*this.speed;
		this.life--;
		if(this.life<=0)
		{
			Game.player.detachEnergies(this);
			return;
		}
	}
	
	public void render(Graphics g)
	{
		g.setColor(Color.ORANGE);
		g.fillOval(Camera.getRealX(this.getX()), Camera.getRealY(this.getY()), this.width,this.height);
	}
}

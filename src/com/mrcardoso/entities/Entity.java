package com.mrcardoso.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;
import com.mrcardoso.core.World;

public class Entity
{
	protected boolean debugMask = false;
	protected int maskX = 0;
	protected int maskY = 0;
	protected int maskWidth = Game.OZ;
	protected int maskHeight = Game.OZ;
	protected int width;
	protected int height;
	/**
	 * frames the current frame to update
	 */
	protected int frames = 0, framesJump = 0;
	/**
	 * the index of the sprite of the toy
	 */
	protected int index = 0, indexJump = 0;
	/**
	 * the max frames to update before move the toy
	 */
	protected int maxFrames = 10;
	/**
	 * the max sprite index of the toy
	 */
	protected int maxIndex = 0;
	protected boolean jumpFromKey = false;
	protected double x;
	protected double y;
	protected BufferedImage sprite;
	
	public static BufferedImage LEVEUP = Game.spritesheet.getSprite(Game.OZ*8, Game.OZ*3, Game.OZ, Game.OZ);
	public static BufferedImage LIFEPACK = Game.spritesheet.getSprite(Game.OZ*7, Game.OZ*3, Game.OZ, Game.OZ);
	public static BufferedImage ENERGY = Game.spritesheet.getSprite(Game.OZ*6, Game.OZ*3, Game.OZ, Game.OZ);
	public static BufferedImage ELEVATION = Game.spritesheet.getSprite(Game.OZ*9, Game.OZ*3, Game.OZ, Game.OZ);
	public static BufferedImage LOGO = Game.spritesheet.getSprite(Game.OZ*10, Game.OZ*4, Game.OZ*4, Game.OZ*4);
	/**
	 * Simple movement of the puppets in the game
	 */
	protected void walk()
	{
		this.frames++;
		if(this.frames==this.maxFrames)
		{
			this.frames = 0;
			this.index++;
			if(this.index > this.maxIndex)
			{
				this.index = 0;
			}
		}
	}
	
	protected void animateJump() {
		this.framesJump++;
		
		if(this.framesJump == this.maxFrames) {
			this.framesJump = 0;
			this.indexJump++;
			if(this.indexJump > this.maxIndex) {
				this.indexJump = 0;
			}
		}
	}
	
	protected void destroyAnimated(){}
	
	public Entity(int x, int y, int width, int height, BufferedImage sprite)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.sprite = sprite;
	}
	
	public void tick() {}
	
	public void render(Graphics g)
	{
		g.drawImage(this.sprite, Camera.getRealX(this.getX()), Camera.getRealY(this.getY()), null);
		if(this.debugMask)
		{
			g.setColor(Color.BLUE);
			g.fillRect(Camera.getRealX(this.getXMask()), Camera.getRealY(this.getYMask()), this.maskWidth, this.maskHeight);			
		}
	}
	
	public boolean isColliding(Entity collided)
	{
		Rectangle target1 = new Rectangle(this.getXMask(), this.getYMask(), this.maskWidth, this.maskHeight);
		Rectangle target2 = new Rectangle(collided.getXMask(), collided.getYMask(), collided.maskWidth, collided.maskHeight);
		
		return target1.intersects(target2);
	}
	
	public double distanceBetweeen(int x, int y, int xx, int yy)
	{
		return Math.sqrt( (x - xx) * (x - xx) + (y - yy) * (y - yy) );
	}
	
	public void resetJumpFrames() {
		this.framesJump = 0;
		this.indexJump = 0;
	}
	
	/**
	 * Method to destroy the entity when it fall in space out of the map of the game
	 * @return boolean
	 */
	public boolean loseByFallHole()
	{
		if(Game.HAS_GRAVITY)
		{
			if( ((this.getY() + Game.OZ) /Game.OZ) >= World.HEIGHT){
				return true;
			}
		}
		return false;
	}
	
	public void setMask(int maskX,int maskY, int maskWidth, int maskHeight)
	{
		this.maskX = maskX;
		this.maskY = maskY;
		this.maskWidth = maskWidth;
		this.maskHeight = maskHeight;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	public int getX() {
		return (int) this.x;
	}
	public double getRealX()
	{
		return this.x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	public int getY() {
		return (int) this.y;
	}
	public double getRealY()
	{
		return this.y;
	}
	
	public int getXMask() {
		return this.getX() + this.maskX;
	}
	public int getYMask() {
		return this.getY() + this.maskY;
	}
	
	public int getWidth() {
		return this.width;
	}
	public int getHeight() {
		return this.height;
	}
	
	public void setJumpFromKey(boolean value) {
		this.jumpFromKey = value;
	}
	
	public BufferedImage getSprite()
	{
		return this.sprite;
	}
}

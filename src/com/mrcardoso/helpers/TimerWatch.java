package com.mrcardoso.helpers;

import com.mrcardoso.core.Game;

public class TimerWatch
{
	public final static int MAX_TIME_LIMIT = 60;
	private static int timeLimit = MAX_TIME_LIMIT, timeIncrement = 0;
	
	public static void tick()
	{
		TimerWatch.timeIncrement++;
		
		if(TimerWatch.timeIncrement > 60) {
			TimerWatch.timeIncrement = 0;
			TimerWatch.timeLimit--;
		}
		
		if(TimerWatch.expired()) {
			Game.lose();
		}
	}
	
	public static int getTimeLimit()
	{
		return TimerWatch.timeLimit;
	}
	
	public static void setTimeLimit(int timeLimit)
	{
		TimerWatch.timeLimit = timeLimit;
	}
	
	public static boolean expired()
	{
		if( TimerWatch.timeLimit  <= 0 ) {
			return true;
		}
		return false;
	}
	
	public static void reset()
	{
		TimerWatch.setTimeLimit(MAX_TIME_LIMIT);
	}
}

package com.mrcardoso.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import com.mrcardoso.entities.Coin;
import com.mrcardoso.entities.Enemy;
import com.mrcardoso.entities.Energy;
import com.mrcardoso.entities.Entity;
import com.mrcardoso.entities.LifePack;
import com.mrcardoso.entities.Player;
import com.mrcardoso.graphcs.Animation;
import com.mrcardoso.helpers.TimerWatch;

public class Storage
{
	private static final String CODE_SLEVEL = "level";
	private static final String CODE_SLIFE = "life";
	private static final String CODE_SENERGY = "energy";
	private static final String CODE_STIME = "timer";
	private static final String CODE_SLEVELALLOW = "leveallow";
	
	private static final String CODE_SLIFEPACK = "lifepack";
	private static final String CODE_COINPACK = "coinpack";
	private static final String CODE_SENERGYPACK = "energypack";
	private static final String CODE_SENEMIES = "enemies";
	
	public static final int MAXSPACE = 3;
	private static String FOLDER = "engine-storage";
	private static String FILE_EXT = ".txt";
	private static final int ENCODE = 100;
	
	private String getFilePath(String filename)
	{
		filename = filename.replace("load", "save");
		return Storage.getBasePath() + FOLDER+ File.separator + filename + FILE_EXT;
	}
	
	private boolean save(ArrayList<String> keys, ArrayList<String> values, String filename)
	{
		int encode = Storage.ENCODE;
		BufferedWriter writer = null;
		try {
			File f = new File(Storage.getBasePath()+FOLDER);
			if(!f.exists()) {
				f.mkdir();
			}
			
			writer = new BufferedWriter(new FileWriter(this.getFilePath(filename)));
			
			for(int i = 0; i < keys.size(); i++) {
				String current = keys.get(i);
				current += ":";
				char[] value = values.get(i).toCharArray();
				for(int j = 0; j < value.length;j++) {
					value[j] += encode;
					current += value[j];
				}
				try {
					writer.write(current);
					if( i < keys.size() - 1) {
						writer.newLine();
					}
				} catch(IOException e) {
					Animation.setFlashMessage("Error write file '"+filename+"'", Animation.FLASH_ERROR);
					break;
				}
			}
			
			try {
				writer.flush();
				writer.close();
				Animation.setFlashMessage("Saving File '"+filename+"'", Animation.FLASH_SUCCESS);
				return true;
			} catch(IOException e) {
				Animation.setFlashMessage("Error close file '"+filename+"'", Animation.FLASH_ERROR);
			}
			
		} catch(IOException e) {
			Animation.setFlashMessage("Error open file '"+filename+"'", Animation.FLASH_ERROR);
		}
		
		return false;
	}
	
	private String load(String filename)
	{
		int encode = Storage.ENCODE;
		String line = "";
		File file = new File(this.getFilePath(filename));
		if(file.exists()) {
			try {
				String singleLine = null;
				BufferedReader reader = new BufferedReader(new FileReader(this.getFilePath(filename)));
				try {
					while((singleLine = reader.readLine()) != null) {
						String[] arrays = singleLine.split(":");
						char[] value = arrays[1].toCharArray();
						arrays[1] = "";
						for(int i = 0; i < value.length; i++) {
							value[i] -= encode;
							arrays[1] += value[i];
						}
						line += arrays[0] + ":"+arrays[1]+"/";
					}
				} catch(IOException e) {
					System.out.println(e.getMessage());
				}
			} catch(FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
		}
		
		return line;
	}
	
	public boolean store(String filename) {
		Player p = Game.player;
		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		keys.addAll(Arrays.asList(CODE_SLEVEL, CODE_SLIFE, CODE_SENERGY, CODE_SLEVELALLOW, CODE_STIME));
		values.addAll(Arrays.asList(
				Integer.toString(Game.getLevel()), 
				Integer.toString((int)p.getLife()),
				Integer.toString(p.getEnergy()),
				Boolean.toString(Game.levelup.isAllowed()),
				Integer.toString(TimerWatch.getTimeLimit())
		));
		
		for(int i = 0; i < Game.entities.size();i++) {
			Entity e = Game.entities.get(i);
			if(e instanceof Enemy) {
				keys.add(CODE_SENEMIES);
				values.add(e.getX() +"x"+e.getY());
			}
			else if(e instanceof Energy) {
				keys.add(CODE_SENERGYPACK);
				values.add(e.getX() +"x"+e.getY());
			}
			else if(e instanceof LifePack) {
				keys.add(CODE_SLIFEPACK);
				values.add(e.getX() +"x"+e.getY());
			}
			else if(e instanceof Coin)
			{
				keys.add(CODE_COINPACK);
				values.add(e.getX() +"x"+e.getY());
			}
		}
		
		return this.save(keys, values, filename);
	}
	
	public void carrer(String filename)
	{
		World.resetStores();
		String str = this.load(filename);
		String[] keys = str.split("/");
		
		int energy = 0;
		int timeLimit = TimerWatch.MAX_TIME_LIMIT;
		double life = Game.player.getMaxLife();
		
		if(!str.isEmpty() && keys.length > 0)
		{
			for(int i = 0; i < keys.length;i++) {
				String[] values = keys[i].split(":");
				String current = values[1];
				switch(values[0]) {
					case Storage.CODE_SLEVEL:
						Game.setLevel(Integer.parseInt(current));
						break;
					case Storage.CODE_SLIFE:
						life = Double.parseDouble(current);
						break;
					case Storage.CODE_SENERGY:
						energy = Integer.parseInt(current);
						break;
					case Storage.CODE_STIME:
						timeLimit = Integer.parseInt(current);
						break;
					case Storage.CODE_SENEMIES:
						World.storesEnemy +=1;
						break;
					case Storage.CODE_SENERGYPACK:
						World.storesEnergy.add(current);
						break;
					case Storage.CODE_SLIFEPACK:
						World.storesLife.add(current);
						break;
					case Storage.CODE_COINPACK:
						World.storesCoin.add(current);
						break;
				}
			}
			World.hasStorage = true;
		}
		Game.setInfiniteEnergy(false);
		Game.reset(true);
		TimerWatch.setTimeLimit(timeLimit);
		Game.levelup.setAllowed(true);
		Game.player.setLife(life);
		Game.player.setEnergy(energy);
		Game.getState().setStateRunning();
	}
	
	public void detech(String filename)
	{
		File file = new File(this.getFilePath(filename));
		if(file.exists()) {
			file.delete();
		}
	}
	public boolean fileExist(String filename) {
		File file = new File(this.getFilePath(filename));
		return file.exists();
	}
	
	public static String getBasePath()
	{
		return System.getProperty("user.home")+File.separator;
	}
}

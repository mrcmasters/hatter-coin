package com.mrcardoso.core;

public class Camera {
	public static int x;
	public static int y;
	
	public static int clamp(int currentScale, int minScale, int maxScale)
	{
		if(currentScale < minScale) {
			currentScale = minScale;
		}
		if(currentScale > maxScale) {
			currentScale = maxScale;
		}
		return currentScale;
	}
	
	public static int getRealX(int x)
	{
		return (x - Camera.x);
	}
	
	public static int getRealY(int y)
	{
		return (y - Camera.y);
	}
}

package com.mrcardoso.core;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.mrcardoso.entities.Boss;
import com.mrcardoso.entities.Coin;
import com.mrcardoso.entities.Elevation;
import com.mrcardoso.entities.Enemy;
import com.mrcardoso.entities.Energy;
import com.mrcardoso.entities.LifePack;
import com.mrcardoso.entities.Particle;
import com.mrcardoso.tiles.Tile;
import com.mrcardoso.tiles.FloorTile;
import com.mrcardoso.tiles.BackGroundTile;

public class World
{
	private boolean enabledMiniMap = false;
	//private MiniMap minimap;
	private static Tile[] tiles;
	
	public static final int HEX_WALL = 0xFFFFFFFF; // white
	public static final int HEX_WALL_TOP = 0xFF999999;
	public static final int HEX_WALL_LEFT = 0xFF666666;
	public static final int HEX_WALL_RIGHT = 0xFF333333;
	public static final int HEX_FLOOR = 0xFF000000; // black
	public static final int HEX_LAVA = 0xFFB00C19; // darkRed
	
	public static final int HEX_CLOUD = 0xFF827F87; // grey 
	public static final int HEX_STAR = 0xFFD1CE7E; // lightYellow 
	public static final int HEX_SUN = 0xFFCD5D02; // orange
	public static final int HEX_MOON = 0xFF87C6CA; // ice
	public static final int HEX_EYE = 0xFF937246; // lightBrown
	
	public static final int HEX_PLAYER = 0xFF00FF00; //green
	public static final int HEX_ENEMY = 0xFFFF0000; // red
	public static final int HEX_BOSS = 0xFFEF4BAB;
	public static final int HEX_LIFE = 0xFFCC0E56; // lightRed
	public static final int HEX_ENERGY = 0xFF7B399D; // lightPurple
	public static final int HEX_COIN = 0xFFFCEF2F; // ligthYellow
	public static final int HEX_LEVELUP = 0xFF501BAF; // purple
	public static final int HEX_ELEVATION = 0xFF0000FF; // blue
	
	public static boolean hasStorage = false;
	public static int WIDTH, HEIGHT;
	public static int storesEnemy = 0;
	public static ArrayList<String> storesEnergy = new ArrayList<String>();
	public static ArrayList<String> storesLife = new ArrayList<String>();
	public static ArrayList<String> storesCoin = new ArrayList<String>();
	
	private boolean canRenderEnemyTile()
	{
		if(World.storesEnemy > 0) {
			return (World.storesEnemy > Game.player.getEnemyTotal());
		}
		return World.hasStorage ? false : true;
		 
	}
	private boolean canRenderTile(int xx, int yy, int tileType)
	{
		ArrayList<String> array = new ArrayList<String>();
		
		switch(tileType) {
			case HEX_ENERGY: array = World.storesEnergy; break;
			case HEX_LIFE: array = World.storesLife; break;
			case HEX_COIN: array = World.storesCoin; break;
		}
		
		if(array.size() > 0) {
			for(int xl = 0; xl < array.size();xl++) {
				
				if(array.get(xl).equals(xx+"x"+yy)) {
					return true;
				}
			}
		} else {
			return World.hasStorage ? false : true;
		}
		return false;
	}
	
	public World(String path)
	{
		try {
			BufferedImage map = ImageIO.read(getClass().getResource(path));
			
			World.WIDTH = map.getWidth();
			World.HEIGHT = map.getHeight();
			//this.minimap = new MiniMap();
			
			int[] pixels = new int[World.WIDTH * World.HEIGHT];
			World.tiles = new Tile[World.WIDTH * World.HEIGHT];
			
			map.getRGB(0, 0, map.getWidth(), map.getHeight(), pixels, 0, map.getWidth());
			
			for(int xx = 0; xx < World.WIDTH;xx++) {
				for(int yy = 0; yy < World.HEIGHT; yy++) {
					int pixelIndex = (xx + (yy * World.WIDTH));
					int pixel = pixels[pixelIndex];
					
					int tilePixelX = xx * Game.OZ;
					int tilePixelY = yy * Game.OZ;
					
					World.tiles[pixelIndex] = new BackGroundTile(tilePixelX, tilePixelY, World.HEX_FLOOR);
					
					switch(pixel) {
						case HEX_WALL:
						case HEX_WALL_TOP:
						case HEX_WALL_LEFT:
						case HEX_WALL_RIGHT:
							World.tiles[pixelIndex] = new FloorTile(tilePixelX, tilePixelY, pixel);
							break;
						case HEX_FLOOR:
//						case HEX_CLOUD:
//						case HEX_SUN:
//						case HEX_MOON:
							World.tiles[pixelIndex] = new BackGroundTile(tilePixelX, tilePixelY, pixel);
							break;
						case HEX_PLAYER:
							Game.player.setX(tilePixelX);
							Game.player.setY(tilePixelY);
							break;
						case HEX_BOSS:
							Game.entities.add(new Boss(tilePixelX, tilePixelY));
							break;
						case HEX_ENEMY:
							if(this.canRenderEnemyTile())
							{
								Enemy en = new Enemy(tilePixelX, tilePixelY);
								Game.entities.add(en);
								Game.enemies.add(en);
								Game.player.increaseEnemyTotal();
							}
							break;
						case HEX_LIFE:
							if(this.canRenderTile(tilePixelX, tilePixelY, HEX_LIFE))
							{
								Game.entities.add(new LifePack(tilePixelX, tilePixelY));
								Game.player.increaseLifeTotal();							
								// Disabled this resource
								//this.minimap.attachPossibility(pixelIndex, World.HEX_LIFE);
							}
							break;
						case HEX_ENERGY:
							if(this.canRenderTile(tilePixelX, tilePixelY, HEX_ENERGY))
							{
								Game.entities.add(new Energy(tilePixelX, tilePixelY));
								Game.player.increaseEnergyTotal();
								//Disabled this resource
								//this.minimap.attachPossibility(pixelIndex, World.HEX_ENERGY);
							}
							break;
						case HEX_COIN:
							if(this.canRenderTile(tilePixelX, tilePixelY, HEX_COIN)) {
								Game.entities.add(new Coin(tilePixelX, tilePixelY));
								Game.player.increaseCoinTotal();
								//Disabled this resource
								//this.minimap.attachPossibility(pixelIndex, World.HEX_COIN);
							}
							break;
						case HEX_ELEVATION:
							Game.entities.add(new Elevation(tilePixelX, tilePixelY));
							//Disabled this resource
							//this.minimap.attachPossibility(pixelIndex, World.HEX_ELEVATION);
							break;
						case HEX_LEVELUP:
							Game.levelup.setX(tilePixelX);
							Game.levelup.setY(tilePixelY);
							break;
					}
				}
			}
			
			World.resetStores();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean isFree(int xnext, int ynext)
	{
		int x1 = xnext/Game.OZ;
		int y1 = ynext/Game.OZ;
		int x2 = (xnext + Game.OZ - 4) / Game.OZ;
		int y2 = (ynext + Game.OZ - 4) /Game.OZ;
		
		if( (xnext < 0 || ynext < 0) || (x1 >= World.WIDTH || y1 >= World.HEIGHT || x2 >= World.WIDTH || y2 >= World.HEIGHT) ) {
			return false;
		}
		
		return !(
			(World.tiles[(x1 + (y1 * World.WIDTH))] instanceof FloorTile) ||
			(World.tiles[(x2 + (y1 * World.WIDTH))] instanceof FloorTile) ||
			(World.tiles[(x1 + (y2 * World.WIDTH))] instanceof FloorTile) ||
			(World.tiles[(x2 + (y2 * World.WIDTH))] instanceof FloorTile)
		);
	}
	
	public static void resetStores()
	{
		World.storesEnergy = new ArrayList<String>();
		World.storesCoin = new ArrayList<String>();
		World.storesLife = new ArrayList<String>();
		World.storesEnemy = 0;
		World.hasStorage = false;
		Game.player.initialSizes();
	}
	
	public static void gererateParticles(int amount, int x, int y, Color color) {
		for(int i =0; i < amount; i++) {
			Game.entities.add(new Particle(x,y, 1, 1, color));
		}
	}
	
	public void tick()
	{
		/*
		this.time++;
		
		if(this.time == 60*10)
		{
			int xnext = Game.rand.nextInt(Game.WIDTH);
			int ynext = Game.rand.nextInt(Game.HEIGHT);
			System.out.println("xnext: "+ xnext +", ynext: "+ynext + "size: "+World.tiles.length);
			if(World.isFree(xnext, ynext)){
				Game.entities.add(new Energy(xnext, ynext, Game.OZ, Game.OZ, Entity.ENERGY));
			}
			this.time = 0;
		}
		*/
	}
	
	public void render(Graphics g)
	{
		int quarter = (int) Math.sqrt(Game.OZ);
		int xstart = Camera.x >> quarter;
		int ystart = Camera.y >> quarter;
		
		int xend = xstart + (Game.WIDTH >> quarter);
		int yend = ystart + (Game.HEIGHT >> quarter);
		//System.out.println("CameraX:"+Camera.x+", Xstart: "+ xstart  + ", XEND: "+ xend);
		
		for(int xx = xstart; xx <= xend;xx++) {
			for(int yy = ystart; yy <= yend; yy++) {
				if( xx < 0 || yy < 0 || xx >= World.WIDTH || yy >= World.HEIGHT) {
					continue;
				}
				Tile tile = World.tiles[(xx + (yy * World.WIDTH))];
				if(tile != null) {
					tile.render(g);
				}	
			}
		}
		//Disabled this resource
		//if(this.enabledMiniMap) {
		//	this.minimap.render();			
		//}
	}
	
	public static Tile getTiles(int position) {
		if( position >= 0 && position < tiles.length) {
			return tiles[position];
		}
		return null;
	}
	public void setEnabledMiniMap(boolean enabledMiniMap) {
		this.enabledMiniMap = enabledMiniMap;
	}
	public boolean isEnabledMiniMap() {
		return this.enabledMiniMap;
	}
	/**
	 * Disabled this resource
	public MiniMap getMiniMap() {
		return this.minimap;
	}
	*/
	
}

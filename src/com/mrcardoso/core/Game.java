package com.mrcardoso.core;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.mrcardoso.entities.Enemy;
import com.mrcardoso.entities.Entity;
import com.mrcardoso.entities.LevelUp;
import com.mrcardoso.entities.Particle;
import com.mrcardoso.entities.Player;
import com.mrcardoso.graphcs.Animation;
import com.mrcardoso.graphcs.Menu;
import com.mrcardoso.graphcs.SpriteSheet;
import com.mrcardoso.graphcs.UI;
import com.mrcardoso.helpers.TimerWatch;
import com.mrcardoso.screens.Screen;
import com.mrcardoso.sound.Sound;
import com.mrcardoso.tiles.Tile;

public class Game extends Canvas implements Runnable, KeyListener, MouseListener, MouseMotionListener {
	
	private static final long serialVersionUID = 1L;
	
	private boolean isRunning = true;
	private UI ui;
	private JFrame frame;
	private Thread thread;
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	
	private static int level = 1;
	private static int mouseX,mouseY;
	private static Game instance;
	private static Menu menu;
	private static States state;
	private static Storage storage;
	
	public final static int OZ = 16;
	public final static int SCALE = 3;
	public final static int WIDTH = 240;
	public final static int HEIGHT = 160;
	public final static int MAX_LEVEL = 10;
	public final static int GRAVITY = 2;
	public final static boolean HAS_GRAVITY = true;
	public final static int DIFFICULTY_EASY = 1;
	public final static int DIFFICULTY_NORMAL = 2;
	public final static int DIFFICULTY_HARD = 3;
	public final static String NAME = "HatterGuy";
	public final static String AUTHOR = "Develop by @MarDozux";
	public final static Dimension ABS_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	
	public final static int SCALE_2 = Game.multiScale(2);
	public final static int SCALE_3 = Game.multiScale(3);
	public final static int SCALE_4 = Game.multiScale(4);
	public final static int SCALE_6 = Game.multiScale(6);
	public final static int SCALE_8 = Game.multiScale(8);
	public final static int SCALE_10 = Game.multiScale(10);
	public final static int SCALE_12 = Game.multiScale(12);
	public final static int SCALE_14 = Game.multiScale(14);
	public final static int SCALE_16 = Game.multiScale(16);
	public final static int SCALE_18 = Game.multiScale(18);
	public final static int SCALE_20 = Game.multiScale(20);
	public final static int SCALE_22 = Game.multiScale(22);
	public final static int SCALE_24 = Game.multiScale(24);
	public final static int SCALE_26 = Game.multiScale(26);
	public final static int SCALE_28 = Game.multiScale(28);
	public final static int SCALE_30 = Game.multiScale(30);
	public final static int SCALE_40 = Game.multiScale(40);
	public final static int SCALE_50 = Game.multiScale(50);
	public final static int SCALE_55 = Game.multiScale(55);
	public final static int SCALE_60 = Game.multiScale(60);
	
	public static boolean validframes = false;
	public static boolean isLooser = false;
	public static boolean isFullScreen = false;
	public static boolean enabledBGSong = true;
	public static boolean enabledEffectSong = true;
	public static boolean hasTime = false;
	private static boolean infiniteEnergy = false;
	private static int difficulty = 2;
	private static int score = 0;
	private static int enemyKill = 0;
	private static int glitchCreditsIndex = 0, glitchLevelsIndex = 0, glichEnergyIndex = 0, glitchMaxIndex = 10;
	
	public static List<Entity> entities;
	public static List<Enemy> enemies;
	public static Random rand;
	public static World world;
	public static SpriteSheet spritesheet;
	public static Image background;
	public static Player player;
	public static LevelUp levelup; 
	public static Screen screens;
	
	public static void main(String[] args)
	{
		Game game = new Game();
		game.start();
		
		Game.storage = new Storage();
		Game.menu = new Menu();
		Game.state = new States();
		Game.screens = new Screen();
	}
	
	public Game()
	{
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		createSizeMode();
		createFrame();
		Game.setInfiniteEnergy(false);
		Game.rand = new Random();
		Game.spritesheet = new SpriteSheet("/spritesheet.png");
		this.ui = new UI();
		Sound.ready.play();
		Game.reset(true);
	}
	
	protected void createFrame()
	{
		frame = new JFrame(Game.NAME);
		frame.add(this);
//		frame.setUndecorated(true); 
		frame.setResizable(false);
		try {
			Image image = ImageIO.read(getClass().getResource("/icon.png"));
			frame.setIconImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		refrashScreen();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	protected void createSizeMode()
	{
		if(Game.isFullScreen) {
			setPreferredSize(new Dimension(Game.ABS_SIZE));			
		} else {
			setPreferredSize(new Dimension(Game.getXcale(), Game.getYcale()));
		}
	}
	
	protected void refrashScreen()
	{
		if(frame!=null) {
			frame.pack();
			frame.setLocationRelativeTo(null);
		}
	}
	
	protected static String imageUtilsPath(String type) {
		String filename = type+"-"+Game.level+".png";
		return "/levels/"+type+ "/"+filename;
	}
	/**
	 * Method called into Storage and Menu classes 
	 * @param resetScore
	 */
	public static void reset(boolean resetScore)
	{
		if(resetScore) {
			Game.resetScore();
		}
		
		if(Game.level>Game.MAX_LEVEL) {
			Game.level = 1;
			Game.credits();
			return;
		}
		TimerWatch.reset();
		Tile.loadSprite();
		
		Game.isLooser = false;
		
		Game.entities = new ArrayList<Entity>();
		Game.enemies = new ArrayList<Enemy>();
		Game.player = new Player();
		Game.levelup = new LevelUp();
		Game.entities.add(Game.player);
		Game.world = new World(Game.imageUtilsPath("world"));
		Game.background = new SpriteSheet(Game.imageUtilsPath("background")).getSprite(0, 0, WIDTH,HEIGHT);
		
		Game.player.updateCamera();
	}
	
	public static void credits() {
		Game.glitchCreditsIndex = 0;
		Animation.resetTyping();
		Game.screens.setScreen(Menu.CODE_MORE_CREDITS);
		Game.getState().setStateScreens();
	}
	
	public static void lose()
	{
		if(Game.difficulty == Game.DIFFICULTY_HARD) {
			Game.resetScore();
		}
		Sound.lose.play();
		Game.isLooser = true;
		Player.renderSpriteLose();
		World.gererateParticles(200, Game.player.getX(), Game.player.getY(), Color.RED);
		Game.state.setStateLose();
	}
	
	public void tick()
	{
		if(Game.state==null) {
			return;
		}
		if(Game.state.isStateMenu()) {
			Game.menu.tick();
		}
		else if(Game.state.isStateRunning()) {
			for(int i = 0; i< Game.entities.size(); i++){
				Entity e = Game.entities.get(i);
				e.tick();
			}
			
			if(Game.levelup.isAllowed()) {
				if(Game.player.isColliding(Game.levelup)) {
					Game.incrementScore();
					Game.incrementLevel();
					
					if(Game.level <= Game.MAX_LEVEL) {
						Game.screens.setScreen(Menu.CODE_MORE_LEVEL);
						Game.state.setStateScreens();
					} else {
						Game.reset(false);
					}
				}
			}
			
			if(Game.hasTime) {
				TimerWatch.tick();
			}
			//Game.world.tick();				
		}
		else if(Game.state.isStateLose()) {
			
			for(int i = 0; i< Game.entities.size(); i++)
			{
				Entity entity = Game.entities.get(i);
				if(entity instanceof Particle) {
					entity.tick();
				} else {
					Game.entities.remove(entity);
				}
			}
		}
	}
	
	public void render(){
		BufferStrategy bs = getBufferStrategy();
		if(bs == null) {
			createBufferStrategy(3);
			return;
		}
		if(Game.state == null) {
			return;
		}
		Graphics g = image.getGraphics();
		Color bgColor = new Color(150,177,234);
			
		g.setColor(bgColor);
		g.fillRect(0, 0, WIDTH,HEIGHT);
		g.drawImage(Game.background, 0, 0, null);
		
		if(Game.state.isStateRunning()) {
			/*Renderiza jogo*/
			Game.world.render(g);
			Game.levelup.render(g);
			
			for(int i = 0; i< Game.entities.size(); i++)
			{
				Entity e = Game.entities.get(i);
				e.render(g);
			}
		}
		
		g.dispose();
		
		g = bs.getDrawGraphics();
		g.drawImage(this.image, 0, 0, Game.getXcale(), Game.getYcale(), null);
		g.setColor(bgColor);

		if(Game.state.isStateInit()) {
			Game.menu.initb(g);
		} else if(Game.state.isStateMenu()) {
			Game.menu.render(g);
		} else if(Game.state.isStateRunning()) {
			if(!Game.isHardMode())
				ui.render(g);
		} else if(Game.state.isStateLose()) {
			Game.screens.setScreen(Menu.CODE_MORE_LOSE);
			Game.screens.render(g);
		} else if(Game.state.isStateScreens()) {
			Game.screens.render(g);
		}
		
		/**
		 * Disabled this resource
		if(Game.world.isEnabledMiniMap())
		{
			((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, Game.world.getMiniMap().getTransparency()));
			int wm = Game.SCALE_50;
			int hm = Game.SCALE_50;
			int wpos = (int) (Game.getXcale()/ ( Game.isFullScreen ? 2.2 : 2.5));
			int hpos = (int)(Game.getYcale() - ( hm + (Game.isFullScreen ? Game.SCALE_20 : Game.SCALE_10 ) ) );
		    g.drawImage(Game.world.getMiniMap().getSpritesheet(), wpos, hpos, wm,hm,null);			
		}
		*/
		
		Animation.flashBlock(g, Game.getXcale()/3, Game.getYcale()-Game.SCALE_4);
		
		bs.show();
	}
	
	public synchronized void start() {
		thread = new Thread(this);
		isRunning = true;
		thread.start();
	}
	
	public synchronized void stop()
	{
		this.isRunning = false;
		try {
			this.thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void run()
	{
		this.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		int frames = 0;
		double timer = System.currentTimeMillis();
		
		while(isRunning) {
			long now = System.nanoTime();
			delta += (now-lastTime) / ns;
			lastTime = now;
			if(delta>=1) {
				tick();
				render();
				frames++;
				delta = 0; //delta--;
			}
			
			if(System.currentTimeMillis() - timer >= 1000) {
//				System.out.println("FPS: "+frames);
				Game.validframes = (frames >= 50 && frames <= 60 ? true : false);
				frames = 0;
				timer += 1000;
			}
		}
		stop();
	}
	
	public static float formatValue(double value, String mask) {
		DecimalFormat formatter = new DecimalFormat(mask);
		return Float.valueOf(formatter.format(value));
	}
	
	public static String getDifficultyLabel() {
		return (Game.difficulty == Game.DIFFICULTY_EASY ? "Easy" : (Game.difficulty == Game.DIFFICULTY_NORMAL ? "Normal" : "Hard")); 
	}
	
	public static void setInfiniteEnergy(boolean value) {
		Game.infiniteEnergy = value;
	}
	public static boolean isInfiniteEnergy() {
		return Game.infiniteEnergy;
	}

	public static States getState()
	{
		return Game.state;
	}
	public static Menu getMenu()
	{
		return Game.menu;
	}
	public static Storage getStorage()
	{
		return Game.storage;
	}
	public static Game getInstance()
	{
		return instance;
	}
	public static int getLevel() {
		return Game.level;
	}
	
	public static boolean isLastLevel() {
		return Game.level == Game.MAX_LEVEL;
	}
	
	public static int getScore() {
		return Game.score;
	}
	
	public static int getEnemyKill() {
		return Game.enemyKill;
	}
	
	public static int getDifficulty() {
		return Game.difficulty;
	}
	
	public static boolean isHardMode() {
		return Game.difficulty == Game.DIFFICULTY_HARD;
	}
	
	public static boolean isNormalMode() {
		return Game.difficulty == Game.DIFFICULTY_NORMAL;
	}
	
	public static boolean isEasyMode() {
		return Game.difficulty == Game.DIFFICULTY_EASY;
	}
	
	public static void setDifficulty(int difficulty) {
		Game.difficulty = difficulty;
	}
	
	public static int getXcale()
	{
		if(Game.isFullScreen) {
			return Game.ABS_SIZE.width;			
		} else {			
			return WIDTH*SCALE;
		}
	}
	public static int getYcale()
	{
		if(Game.isFullScreen) {
			return Game.ABS_SIZE.height;			
		} else {			
			return HEIGHT*SCALE;
		}
	}
	
	public static int getMouseX() {
		return Game.mouseX;
	}
	
	public static int getMouseY() {
		return Game.mouseY;
	}
	
	public static int multiScale(int value)
	{
		if(Game.isFullScreen) {
			return value;
		} else{
			return Game.SCALE*value;			
		}
	}
	
	public static void incrementScore() {
		if(Game.player!=null) {
			Game.score += Game.player.getAccumulatedCoins();
			Game.enemyKill += Game.player.getAccumulatedEnemy();
		}
	}
	
	public static void resetScore() {
		Game.score = 0;
		Game.enemyKill = 0;
	}
	
	public static void incrementLevel()
	{
		Game.level++;
	}
	public static void setLevel(int level) {
		Game.level = level;
	}
	
	/**
	 * ---------------------------------------------------------------------------
	 *  KEYBOARD EVENTS
	 *  ---------------------------------------------------------------------------
	 */
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if(Game.state.isStateRunning()) {
				Game.player.setRightSide(true);
			} else if(Game.state.isStateMenu()) {
				Game.menu.setRight(true);
			}
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			if(Game.state.isStateRunning()) {
				Game.player.setRightSide(false);
			} else if(Game.state.isStateMenu()) {
				Game.menu.setLeft(true);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			if(Game.state.isStateMenu()) {
				Game.menu.setUp(true);
			}
		} else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			if(Game.state.isStateMenu()) {
				Game.menu.setDown(true);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			if(Game.state.isStateRunning()) {
				if(Game.player.isInFloor()) {
					Sound.jumping.play();
					Game.player.resetJumpFrames();
					Game.player.startJump(true);
				}
				Game.player.setJumpFromKey(true);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_S) {
			if(Game.state.isStateRunning()) {
				Game.player.setIsAttacking(true);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_F) {
			Game.isFullScreen = !(Game.isFullScreen);
			this.createSizeMode();
			this.refrashScreen();
		}
		
		if(e.getKeyCode() == KeyEvent.VK_X) {
			if(Game.state.isStateMenu()) {
				Game.menu.setDeleting(true);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			if(Game.state.isStateMenu()) {
				Game.menu.setChoosed(true);
			} else if(Game.state.isStateLose()) {
				Game.state.setStateMenu();
				Game.menu.setPaused(false);
			} else if(Game.state.isStateScreens() && Game.screens.isLevelScreen()) {
				Game.reset(false);
				Game.getState().setStateRunning();
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if(Game.state.isStateRunning()) {
				Game.state.setStateMenu();
				Game.menu.setPaused(true);
			} else if(Game.state.isStateMenu()) {
				Game.menu.setAllowSubItems(false);
			} else if(Game.state.isStateScreens() && !Game.screens.isLevelScreen()) {
				Game.state.setStateMenu();
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_C) {
			if(Game.state.isStateMenu()) {
				Game.glitchCreditsIndex++;
				if(Game.glitchCreditsIndex == Game.glitchMaxIndex) {
					Game.credits();
				}
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_L) {
			if(Game.state.isStateMenu()) {
				Game.glitchLevelsIndex++;
				if(Game.glitchLevelsIndex == Game.glitchMaxIndex) {
					Game.glitchLevelsIndex = 0;
					Game.menu.setChooseLevel();
				}
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_K) {
			if(Game.state.isStateMenu()) {
				Game.glichEnergyIndex++;
				if(Game.glichEnergyIndex == Game.glitchMaxIndex) {
					Game.glichEnergyIndex = 0;
					Game.setInfiniteEnergy(!Game.isInfiniteEnergy());
				}
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			Game.player.setRight(false);
		} else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			Game.player.setLeft(false);
		}
	}
	
	public void keyTyped(KeyEvent e) { }
	
	/**
	 * ---------------------------------------------------------------------------
	 *  MOUSE EVENTS
	 *  ---------------------------------------------------------------------------
	 */

	public void mousePressed(MouseEvent e) {
		if(Game.state.isStateRunning())
		{
			if(Game.isFullScreen) {
//				Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
//				Game.player.setMouseAttack( e.getX() / (d.width/ SCALE),  e.getY() /( d.height / SCALE));
//				Game.player.setMouseAttack( e.getX()/ SCALE, e.getY()/ SCALE);
			} else {
				Game.player.setMouseAttack((e.getX() / SCALE), (e.getY() / SCALE));
			}			
		}
	}
	
	public void mouseClicked(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}
	
	/**
	 * ---------------------------------------------------------------------------
	 *  MOUSE MOTION EVENTS
	 *  ---------------------------------------------------------------------------
	 */
	public void mouseDragged(MouseEvent e) {}
	
	public void mouseMoved(MouseEvent e) {
		Game.mouseX = e.getX();
		Game.mouseY = e.getY();
	}

}
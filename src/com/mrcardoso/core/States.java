package com.mrcardoso.core;

import com.mrcardoso.sound.Sound;

public class States
{
	public static final int GAME_STATE_INIT = 0;
	public static final int GAME_STATE_MENU = 1;
	public static final int GAME_STATE_RUNNIN = 2;
	public static final int GAME_STATE_LOSE = 3;
	public static final int GAME_STATE_SCREENS = 4;
	
	private int state = GAME_STATE_MENU;
	
	public States() {
		this.setStateMenu();
	}
	
	public void setStateInit()
	{
		this.state = States.GAME_STATE_INIT;
	}
	public void setStateMenu()
	{
		Sound.credits.stop();
		Sound.bossFight.stop();
		Sound.music.stop();
		
		Sound.menu.loop();
		
		Game.getMenu().setAllowSubItems(false);
		this.state = States.GAME_STATE_MENU;
	}
	public void setStateRunning()
	{
		Sound.menu.stop();
		Sound.credits.stop();
		
		if(Game.isLastLevel()) {
			Sound.music.stop();
			Sound.bossFight.loop();
		} else {
			Sound.bossFight.stop();
			Sound.music.loop();
		}
		this.state = States.GAME_STATE_RUNNIN;
	}
	
	public void setStateLose() {
		this.state = States.GAME_STATE_LOSE;
	}
	
	public void setStateScreens() {
		if(Game.screens.isCreditScreen()) {
			Sound.music.stop();
			Sound.bossFight.stop();
			Sound.credits.play();
		}
		this.state = States.GAME_STATE_SCREENS;
	}
	
	public boolean isStateInit()
	{
		return this.state == States.GAME_STATE_INIT;
	}
	
	public boolean isStateMenu()
	{
		return this.state == States.GAME_STATE_MENU;
	}
	
	public boolean isStateRunning()
	{
		return this.state == States.GAME_STATE_RUNNIN;
	}
	public boolean isStateLose() {
		return this.state == States.GAME_STATE_LOSE;
	}
	
	public boolean isStateScreens() {
		return this.state == States.GAME_STATE_SCREENS;
	}
}

package com.mrcardoso.tiles;


import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.mrcardoso.core.Camera;
import com.mrcardoso.core.Game;

public class Tile
{	
	public static BufferedImage TILE_GROUND_LEFT;
	public static BufferedImage TILE_GROUND_DOWN;
	public static BufferedImage TILE_GROUND_TOP;
	public static BufferedImage TILE_GROUND_RIGHT;
	
	protected BufferedImage sprite;
	protected int x,y;
	protected int maskx = 0, masky = 0;
	
	public Tile(int x, int y, BufferedImage sprite)
	{
		this.x = x;
		this.y = y;
		this.sprite = sprite;
	}
	
	public void render(Graphics g)
	{
		if(this.sprite != null) {
			g.drawImage(this.sprite, Camera.getRealX(this.x), Camera.getRealY(this.y), null);			
		}
	}
	
	public static void loadSprite() {
		int colTop = 0,colDown = 0,colLeft = 0, colRight = 0;
		int rowTop = 2,rowDown = 2,rowLeft = 2, rowRight = 2;
		
		switch(Game.getLevel()) {
			case 1:
				colTop = 0;
				colDown = 1;
				colLeft = 2;
				colRight = 3;
				break;
			case 2:
			case 9:
				colTop = 4;
				colDown = 5;
				colLeft = 6;
				colRight = 7;
				break;
			case 3:
			case 8:
				colTop = 8;
				colDown = 9;
				colLeft = 10;
				colRight = 11;
				break;
			case 4:
				colTop = 12;
				colDown = 13;
				colLeft = 12;
				colRight = 13;
				rowTop = rowDown = 2;
				rowLeft = rowRight = 3;
				break;
			case 6:
			case 7:
				colTop = 14;
				colDown = 14;
				colLeft = 15;
				colRight = 15;
				rowTop = rowLeft = 2;
				rowDown = rowRight = 3;
				break;
			case 5:
			case 10:
				colTop = 14;
				colDown = 14;
				colLeft = 15;
				colRight = 15;
				rowTop = rowLeft = 4;
				rowDown = rowRight = 5;
				break;
		}
		
		TILE_GROUND_TOP = Game.spritesheet.getSprite(Game.OZ*colTop, Game.OZ*rowTop, Game.OZ, Game.OZ);
		TILE_GROUND_DOWN = Game.spritesheet.getSprite(Game.OZ*colDown, Game.OZ*rowDown, Game.OZ, Game.OZ);
		TILE_GROUND_LEFT = Game.spritesheet.getSprite(Game.OZ*colLeft, Game.OZ*rowLeft, Game.OZ, Game.OZ);
		TILE_GROUND_RIGHT = Game.spritesheet.getSprite(Game.OZ*colRight, Game.OZ*rowRight, Game.OZ, Game.OZ);
	}
}

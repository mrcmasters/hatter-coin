package com.mrcardoso.tiles;

import java.awt.image.BufferedImage;

import com.mrcardoso.core.World;

public class FloorTile extends Tile{
	public FloorTile(int x, int y, int type) {
		super(x, y, getTileSprite(type));
	}
	
	private static BufferedImage getTileSprite(int type)
	{
		BufferedImage sprite;
		
		switch(type) {
			case World.HEX_WALL: sprite = Tile.TILE_GROUND_DOWN; break;
			case World.HEX_WALL_TOP: sprite = Tile.TILE_GROUND_TOP; break;
			case World.HEX_WALL_LEFT: sprite = Tile.TILE_GROUND_LEFT; break;
			case World.HEX_WALL_RIGHT: sprite = Tile.TILE_GROUND_RIGHT; break;
			default: sprite = null; break;
		}
		
		return sprite;
	}
}

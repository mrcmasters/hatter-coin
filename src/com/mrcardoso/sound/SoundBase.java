package com.mrcardoso.sound;

public abstract class SoundBase {
	
	public static String createPath(String name) {
		String ext = "wav";
		String folder = "win";
		String path = "/music/"+folder+"/"+name+"."+ext;
		return path;
	}
	
	public abstract void play();
	public abstract void stop();
	public abstract void loop();
}

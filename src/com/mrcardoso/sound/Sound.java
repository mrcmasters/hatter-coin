package com.mrcardoso.sound;

public class Sound {
	public static final SoundBase music = load("soundcloud",1);
	public static final SoundBase menu = load("Wind",1);
	public static final SoundBase coin = load("coins",1);
	public static final SoundBase explosion = load("laserLarge",1);
	public static final SoundBase jumping = load("jumping",1);
	public static final SoundBase lose = load("game_over",1);
	public static final SoundBase shoot = load("laser",1);
	public static final SoundBase ready = load("prepare_yourself",1);
	public static final SoundBase bossFight = load("bossBattle", 1);
	public static final SoundBase credits = load("credits", 1);
	
	
	private static SoundBase load(String name, int count) {
		return SoundAdvanced.instance(name, count);
	}
}

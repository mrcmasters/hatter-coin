package com.mrcardoso.sound;

import java.applet.Applet;
import java.applet.AudioClip;

import com.mrcardoso.core.Game;

public class SoundLegacy extends SoundBase{
	private AudioClip clip;
	
	public static SoundLegacy instance(String name, int count) {
		return new SoundLegacy(name);
	}
	
	private SoundLegacy(String name) {
		try {
			String path = SoundBase.createPath(name);
			
			clip = Applet.newAudioClip(SoundLegacy.class.getResource(path));
		} catch(Throwable e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void play()
	{
		if(!Game.enabledEffectSong) return;
		try {
			new Thread() {
				public void run ()
				{
					clip.play();
				}
			}.start();
		} catch(Throwable e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void stop() {
		try	{
			new Thread() {
				public void run() {
					clip.stop();
				}
			}.start();
		} catch(Throwable e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void loop()
	{
		if(!Game.enabledBGSong) return;
		try {
			new Thread() {
				public void run ()
				{
					clip.loop();
				}
			}.start();
		} catch(Throwable e) {
			System.out.println(e.getMessage());
		}
	}
}

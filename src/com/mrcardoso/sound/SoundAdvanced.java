package com.mrcardoso.sound;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.mrcardoso.core.Game;

public class SoundAdvanced{
	public static class Clips extends SoundBase{
		public Clip[] clips;
		private int p;
		private int count;
		
		public Clips(byte[] buffer, int count) throws LineUnavailableException, IOException, UnsupportedAudioFileException {
			if(buffer == null)
				return;
			
			clips = new Clip[count];
			this.count = count;
			
			for(int i = 0; i < count; i++) {
				clips[i] = AudioSystem.getClip();
				clips[i].open(AudioSystem.getAudioInputStream(new ByteArrayInputStream(buffer)));
			}
		}
		
		public void play() {
			if(!Game.enabledEffectSong) return;
			if(clips == null) return;
//			if(clips[p].isRunning()) return;
			clips[p].stop();
			clips[p].setFramePosition(0);
			clips[p].start();
			p++;
			if(p>=count) p = 0;
		}
		
		public void stop()
		{
			if(clips == null) return;
			clips[p].stop();
		}
		
		public void loop() {
			if(!Game.enabledBGSong) return;
			if(clips == null) return;
			clips[p].loop(300);
		}

	}
	
	public static Clips instance(String name, int count) {
		return load(name, count);
	}
	
	private static Clips load(String name,int count) {
		try {
			String path = SoundBase.createPath(name);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataInputStream dis = new DataInputStream(SoundAdvanced.class.getResourceAsStream(path));
			
			byte[] buffer = new byte[1024];
			int read = 0;
			while((read = dis.read(buffer)) >= 0) {
				baos.write(buffer,0,read);
			}
			dis.close();
			byte[] data = baos.toByteArray();
			return new Clips(data,count);
		}catch(Exception e) {
			//e.printStackTrace();
			System.out.println("ERR: "+e.getMessage()+ "File: "+name);
			try {
				return new Clips(null,0);
			}catch(Exception ee) {
				return null;
			}
		}
	}

}

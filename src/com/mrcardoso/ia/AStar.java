package com.mrcardoso.ia;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.mrcardoso.core.World;
import com.mrcardoso.tiles.Tile;
import com.mrcardoso.tiles.FloorTile;

public class AStar {
	public static double lastTime = System.currentTimeMillis();
	public static Comparator<Node> nodeSorter = new Comparator<Node>() {
		@Override
		public int compare(Node node0, Node node1)
		{
			if(node1.getfCost() < node0.getfCost()) {
				return +1;
			} else if(node1.getfCost() > node0.getfCost()) {
				return -1;
			}
			return 0;
		}
	};
	
	public static boolean clear()
	{
		if(System.currentTimeMillis() - lastTime >= 1000) {
			return true;
		}
		return false;
	}
	
	public static List<Node> search(World world, Vector2i origin, Vector2i destiny)
	{
		lastTime = System.currentTimeMillis();
		List<Node> openList = new ArrayList<Node>();
		List<Node> closedList = new ArrayList<Node>();
		
		Node current = new Node(origin, null, 0, getDistance(origin,destiny));
		openList.add(current);
		while(openList.size() > 0) {
			Collections.sort(openList, AStar.nodeSorter);
			current = openList.get(0);
			if(current.getTile().equals(destiny))
			{
				List<Node> path = new ArrayList<Node>();
				while(current.getParent() != null) {
					path.add(current);
					current = current.getParent();
				}
				openList.clear();
				closedList.clear();
				return path;
			}
			openList.remove(current);
			closedList.add(current);
			
			for(int i =0;i< 9;i++)
			{
				if(i==4) {
					continue;
				}
				int x = current.getTile().getX();
				int y = current.getTile().getY();
				int xi = (i % 3) -1;
				int yi = (i / 3) -1;
				
				Tile tile = World.getTiles( x + xi + ( (y + yi) * World.WIDTH) );
				if(tile == null || tile instanceof FloorTile) {
					continue;
				}
				
				Tile diagonal1, diagonal2;
				switch(i) {
					case 0:
						diagonal1 = World.getTiles( x + xi +  1 + ( (y + yi) * World.WIDTH) );
						diagonal2 = World.getTiles( x + xi + ( (y + yi + 1) * World.WIDTH) );
						if(diagonal1 instanceof FloorTile || diagonal2 instanceof FloorTile) {
							continue;
						}
						break;
					case 2:
						diagonal1 = World.getTiles( x + xi - 1 + ( (y + yi) * World.WIDTH) );
						diagonal2 = World.getTiles( x + xi + ( (y + yi + 1) * World.WIDTH) );
						if(diagonal1 instanceof FloorTile || diagonal2 instanceof FloorTile) {
							continue;
						}
						break;
					case 6:
						diagonal1 = World.getTiles( x + xi + ( (y + yi - 1) * World.WIDTH) );
						diagonal2 = World.getTiles( x + xi + 1 + ( (y + yi) * World.WIDTH) );
						if(diagonal1 instanceof FloorTile || diagonal2 instanceof FloorTile) {
							continue;
						}
						break;
					case 8: 
						diagonal1 = World.getTiles( x + xi + ( (y + yi - 1) * World.WIDTH) );
						diagonal2 = World.getTiles( x + xi - 1 + ( (y + yi) * World.WIDTH) );
						if(diagonal1 instanceof FloorTile || diagonal2 instanceof FloorTile) {
							continue;
						}
						break;
				}
				
				Vector2i a = new Vector2i(x+xi, y+yi);
				double gCost = current.getgCost() + getDistance(current.getTile(), a);
				double hCost = getDistance(a, destiny);
				
				Node node = new Node(a, current, gCost,hCost);
				
				if(AStar.inVector(closedList, a) && gCost >= current.getgCost()) {
					continue;
				}
				
				if(!AStar.inVector(openList, a)) {
					openList.add(node);
				} else if(gCost < current.getgCost()) {
					openList.remove(current);
					openList.add(node);
				}
			}
		}
		closedList.clear();
		return null;
	}
	
	private static boolean inVector(List<Node> items, Vector2i vector)
	{
		for(int i = 0; i< items.size(); i++)
		{
			if(items.get(i).getTile().equals(vector)) {
				return true;
			}
		}
		return false;  
	}
	private static double getDistance(Vector2i origin, Vector2i destiny)
	{
		double dx = origin.getX() - destiny.getX();
		double dy = origin.getY() - destiny.getY();
		
		return Math.sqrt( dx * dx + dy * dy);
	}
}

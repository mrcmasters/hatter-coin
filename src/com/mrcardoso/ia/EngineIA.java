package com.mrcardoso.ia;

import java.util.List;

import com.mrcardoso.core.Game;
import com.mrcardoso.core.World;
import com.mrcardoso.entities.Enemy;
import com.mrcardoso.entities.Player;

public class EngineIA
{
	public final static String FOLLOW_SIMPLE = "ia_follow_basic";
	public final static String FOLLOW_RIGHT_LEFT = "ia_follow_right_on_left";
	public final static String FOLLOW_ASTAR = "ia_follow_astar";
	private String mode;
	private Enemy enemy;
	
	public EngineIA(Enemy enemy, String mode)
	{
		this.enemy = enemy;
		this.mode = mode;
	}
	
	public void tick()
	{
		Player p = Game.player;
		switch(mode) {
			case FOLLOW_SIMPLE:
				if(this.enemy.getIsMoving()){
					this.pursuit(p);
				}
				break;
			case FOLLOW_ASTAR:
				this.astar(p);
				break;
			case FOLLOW_RIGHT_LEFT: 
				this.rightOnLeft(p);
				break;
		}
	}
	
	private boolean canMove(int xxnext, int yynext)
	{
		return ( World.isFree(xxnext, yynext) && !this.enemy.isColliding(xxnext, yynext) );
	}
	
	private void  rightOnLeft(Player p)
	{
		int xxnext = (int) (this.enemy.nextX());
		int xxprev = (int) (this.enemy.prevX());
		int nexttile = (Game.rand.nextInt(100) > 50 ? (Game.OZ/2) : 0);
		
		if(Game.HAS_GRAVITY) {
			if( World.isFree(this.enemy.getX(), (int) (this.enemy.nextY())) ) {
				this.enemy.increaseY();
			}
		}
		
		if(this.enemy.right) {
			if(World.isFree(xxnext, this.enemy.getY()))
			{
				this.enemy.increaseX();
				
				if( World.isFree((int) (this.enemy.getRealX()+nexttile), (int) (this.enemy.getRealY() + Game.GRAVITY) ) ) {
					this.enemy.left = true;
					this.enemy.right = false;
				}				
			} else {
				this.enemy.left = true;
				this.enemy.right = false;
			}
		}
		if(this.enemy.left) {
			if(World.isFree(xxprev, this.enemy.getY())) {
				this.enemy.decreaseX();
				
				if( World.isFree( (int) (this.enemy.getRealX()-nexttile), (int) (this.enemy.getRealY() + Game.GRAVITY) ) ) {
					this.enemy.left = false;
					this.enemy.right = true;
				}				
			} else {
				this.enemy.left = false;
				this.enemy.right = true;
			}
		}
	}
	
	private void pursuit(Player p)
	{
		int xxnext = (int) (this.enemy.nextX());
		int xxprev = (int) (this.enemy.prevX()); 
		
		int yynext = (int) (this.enemy.nextY());
		int yyprev = (int) (this.enemy.prevY());
		
		if(this.enemy.getX() < p.getX() && this.canMove(xxnext, this.enemy.getY())) {
			this.enemy.increaseX();
		} else if(this.enemy.getX() > p.getX() && this.canMove(xxprev, this.enemy.getY())) {
			this.enemy.decreaseX();
		}
		
		if(Game.HAS_GRAVITY) {
			if( World.isFree(this.enemy.getX(), (int) (this.enemy.nextY())) ) {
				this.enemy.increaseY();
			}
			return;
		}
		
		if(this.enemy.getY() < p.getY() && this.canMove(this.enemy.getX(), yynext)) {
			this.enemy.increaseY();
		} else if(this.enemy.getY() > p.getY() && this.canMove(this.enemy.getX(), yyprev)) {
			this.enemy.decreaseY();
		}
	}
	
	private void astar(Player p)
	{
		List<Node> path = this.enemy.getPath();
		if( (path == null || path.size() == 0))
		{
			Vector2i origin = new Vector2i((int)(this.enemy.getX()/Game.OZ), (int)(this.enemy.getY()/Game.OZ));
			Vector2i destiny = new Vector2i((int)(p.getX()/Game.OZ), (int)(p.getY()/Game.OZ));
			this.enemy.setPath(AStar.search(Game.world, origin, destiny));
		}
		
		if(path != null && path.size() > 0)
		{
			int index = path.size()-1;
			Vector2i target = path.get(index).getTile();
			int rx = target.getX() * Game.OZ;
			int ry = target.getY() * Game.OZ;
			
			boolean xxnext = this.canMove((int) (this.enemy.nextX()), this.enemy.getY());
			boolean xxprev = this.canMove((int) (this.enemy.prevX()), this.enemy.getY()); 
			
			if(this.enemy.getX() < rx && !xxnext) {
				this.enemy.increaseX();
			} else if(this.enemy.getX() > rx && !xxprev) {
				this.enemy.decreaseX();
			}
			
			if(Game.HAS_GRAVITY) {
				if( World.isFree(this.enemy.getX(), (int) (this.enemy.nextY())) ) {
					this.enemy.increaseY();
				}
			}
			else
			{
				boolean yynext = this.canMove(this.enemy.getX(), (int) (this.enemy.nextY()));
				boolean yyprev = this.canMove(this.enemy.getX(), (int) (this.enemy.prevY()));
				
				if(this.enemy.getY() < ry && !yynext) {
					this.enemy.increaseY();
				} else if(this.enemy.getY() > ry && !yyprev) {
					this.enemy.decreaseY();
				}
			}
			
			if(this.enemy.getX() == rx && ( this.enemy.getY() == ry || Game.HAS_GRAVITY) )
			{
				path.remove(index);
			}
			else if(AStar.clear())
			{
				path.clear();
			}
		}
	}
	
	public String getMode() {
		return mode;
	}
}

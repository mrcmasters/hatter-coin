package com.mrcardoso.ia;

public class Vector2i {
	private int x,y;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Vector2i(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public boolean equals(Object object) {
		Vector2i vector = (Vector2i) object;
		
		if(vector.x == this.x && vector.y == this.y) {
			return true;
		}
		
		return false; 
	}
}

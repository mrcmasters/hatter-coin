package com.mrcardoso.movement;

import com.mrcardoso.core.Game;
import com.mrcardoso.core.World;
import com.mrcardoso.entities.Player;

public class Movement {
	
	private static int jumpFrames = 0, jumpMaxFrames = (Game.OZ*3);
	public static void directionXY(Player player)
	{
		if(player.isRight() && World.isFree((int) player.nextX(), player.getY()) )
		{
			player.increaseX();
		}
		else if(player.isLeft() && World.isFree((int) (player.prevX()), player.getY()))
		{
			player.decreaseX();
		}
		
		if(player.isUp() && World.isFree(player.getX(), (int) (player.prevY())))
		{
			player.decreaseY();
		}
		else if(player.isDown() && World.isFree(player.getX(), (int) (player.nextY())))
		{
			player.increaseY();
		}
	}
	
	public static void directionX(Player player)
	{
		if(player.isJumping()) {
			jumpFrames += (Game.HAS_GRAVITY ? Game.GRAVITY : player.speed);
			boolean canUp = jumpFrames < jumpMaxFrames;
			boolean yFree = World.isFree(player.getX(), (int) (player.prevY()));
			if(!yFree || !canUp) {
				player.setJumping(false);
				player.setFalling(true);
				jumpFrames = 0;
			} else {
				player.decreaseY();
			}
		} else {
			if(World.isFree(player.getX(), (int) (player.nextY()))) {
				player.setInFloor(false);
				player.increaseY();
			} else {
				player.setInFloor(true);
				player.setJump(false);
				player.setFalling(false);
				player.setJumpFromKey(false);
			}
		}
//		if(!player.isJumping && World.isFree(player.getX(), (int) (player.nextY())))
//		{
//			player.isFalling = true;
//			player.increaseY();
//		} else{
//			if(player.jump) {
//				player.isJumping = true;
//			}
//			player.isFalling = false;
//		}
//		
//		if(player.isJumping)
//		{
//			if(World.isFree(player.getX(), (int) (player.prevY())))
//			{
//				player.decreaseY();
//				jumpFrames += (Game.HAS_GRAVITY ? Game.GRAVITY : player.speed);
//				if(jumpFrames==jumpMaxFrames) {
//					player.isJumping = false;
//					player.jump = false;
//					jumpFrames = 0;
//				}
//			} else {
//				player.isJumping = false;
//				player.jump = false;
//				jumpFrames = 0;
//			}
//		}
		
		if(player.isRight() && World.isFree((int) player.nextX(), player.getY()) ){
			player.increaseX();
		} else if(player.isLeft() && World.isFree((int) (player.prevX()), player.getY())) {
			player.decreaseX();
		}
	}
}

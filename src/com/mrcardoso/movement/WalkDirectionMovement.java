package com.mrcardoso.movement;

public interface WalkDirectionMovement
{
	static final double SPEED = 0.1;
	static final int MAX_SPEED = 1;
	
	boolean right = false;
	boolean left = false;
	boolean up = false;
	boolean down = false;
	double speed = 0.1;
	boolean jump = false;
	boolean isJumping = false;
	boolean isFalling = false; 
	
	/**
	 * Method to preview the direction of the puppet to the right
	 * @return
	 */
	public double nextX();
	/**
	 * Method to preview the direction of the puppet to the down
	 * @return
	 */
	public double nextY();
	/**
	 * Method to preview the direction of the puppet to the left
	 * @return
	 */
	public double prevX();
	/**
	 * Method to preview the direction of the puppet to the up
	 * @return
	 */
	public double prevY();
	/**
	 * Method to increment the direction of the puppet to the right
	 * @return
	 */
	public void increaseX();
	/**
	 * Method to reduce the direction of the puppet to the left
	 * @return
	 */
	public void decreaseX();
	/**
	 * Method to increment the direction of the puppet to the down
	 * @return
	 */
	public void increaseY();
	/**
	 * Method to reduce the direction of the puppet to the up
	 * @return
	 */
	public void decreaseY();
	/**
	 * Method to increment the speed of the puppet when it is walking
	 */
	public void updateSpeed();
}
